package com.yr.visa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
@ComponentScan({ "com.yr.visa.**" })
@MapperScan("com.yr.visa.core.mapper")
@EnableAutoConfiguration
public class YrVisaStarter {

	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(YrVisaStarter.class);
		app.setBannerMode(Mode.OFF);
		app.run(args);
	}
}
