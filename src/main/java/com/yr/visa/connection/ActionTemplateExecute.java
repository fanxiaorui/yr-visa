package com.yr.visa.connection;

import com.yr.visa.proxy.ctx.Brower;

public interface ActionTemplateExecute<T> {

	public T action(Brower b);
	
}
