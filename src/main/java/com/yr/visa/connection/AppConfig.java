package com.yr.visa.connection;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "appconfig")
public class AppConfig {

	private String phantPath;

	private String loadPageTimeout;

	private String elementGetTimeout;

	private String imageSavePath;
	
	private String imageAccessUrl;
	
	
	

	public String getImageAccessUrl() {
		return imageAccessUrl;
	}

	public void setImageAccessUrl(String imageAccessUrl) {
		this.imageAccessUrl = imageAccessUrl;
	}

	public String getImageSavePath() {
		return imageSavePath;
	}

	public void setImageSavePath(String imageSavePath) {
		this.imageSavePath = imageSavePath;
	}

	public String getPhantPath() {
		return phantPath;
	}

	public void setPhantPath(String phantPath) {
		this.phantPath = phantPath;
	}

	public String getLoadPageTimeout() {
		return loadPageTimeout;
	}

	public void setLoadPageTimeout(String loadPageTimeout) {
		this.loadPageTimeout = loadPageTimeout;
	}

	public String getElementGetTimeout() {
		return elementGetTimeout;
	}

	public void setElementGetTimeout(String elementGetTimeout) {
		this.elementGetTimeout = elementGetTimeout;
	}

}
