package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.BasicInfoExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BasicInfoMapper extends GenericDao {
    int countByExample(BasicInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BasicInfo record);

    int insertSelective(BasicInfo record);

    List<BasicInfo> selectByExample(BasicInfoExample example);

    BasicInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BasicInfo record, @Param("example") BasicInfoExample example);

    int updateByExample(@Param("record") BasicInfo record, @Param("example") BasicInfoExample example);

    int updateByPrimaryKeySelective(BasicInfo record);

    int updateByPrimaryKey(BasicInfo record);
}