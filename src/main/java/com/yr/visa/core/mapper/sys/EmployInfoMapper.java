package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.EmployInfoExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmployInfoMapper extends GenericDao {
    int countByExample(EmployInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(EmployInfo record);

    int insertSelective(EmployInfo record);

    List<EmployInfo> selectByExample(EmployInfoExample example);

    EmployInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") EmployInfo record, @Param("example") EmployInfoExample example);

    int updateByExample(@Param("record") EmployInfo record, @Param("example") EmployInfoExample example);

    int updateByPrimaryKeySelective(EmployInfo record);

    int updateByPrimaryKey(EmployInfo record);
}