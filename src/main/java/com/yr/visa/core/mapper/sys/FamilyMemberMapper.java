package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.FamilyMemberExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FamilyMemberMapper extends GenericDao {
    int countByExample(FamilyMemberExample example);

    int deleteByPrimaryKey(Long id);

    int insert(FamilyMember record);

    int insertSelective(FamilyMember record);

    List<FamilyMember> selectByExample(FamilyMemberExample example);

    FamilyMember selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") FamilyMember record, @Param("example") FamilyMemberExample example);

    int updateByExample(@Param("record") FamilyMember record, @Param("example") FamilyMemberExample example);

    int updateByPrimaryKeySelective(FamilyMember record);

    int updateByPrimaryKey(FamilyMember record);
}