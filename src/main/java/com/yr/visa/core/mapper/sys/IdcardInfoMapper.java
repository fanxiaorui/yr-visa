package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.IdcardInfoExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IdcardInfoMapper extends GenericDao {
    int countByExample(IdcardInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(IdcardInfo record);

    int insertSelective(IdcardInfo record);

    List<IdcardInfo> selectByExample(IdcardInfoExample example);

    IdcardInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") IdcardInfo record, @Param("example") IdcardInfoExample example);

    int updateByExample(@Param("record") IdcardInfo record, @Param("example") IdcardInfoExample example);

    int updateByPrimaryKeySelective(IdcardInfo record);

    int updateByPrimaryKey(IdcardInfo record);
}