package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.Ord;
import com.yr.visa.core.model.sys.OrdExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrdMapper extends GenericDao {
    int countByExample(OrdExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Ord record);

    int insertSelective(Ord record);

    List<Ord> selectByExample(OrdExample example);

    Ord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Ord record, @Param("example") OrdExample example);

    int updateByExample(@Param("record") Ord record, @Param("example") OrdExample example);

    int updateByPrimaryKeySelective(Ord record);

    int updateByPrimaryKey(Ord record);
}