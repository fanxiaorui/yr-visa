package com.yr.visa.core.mapper.sys;

import com.yr.visa.core.model.sys.VisaInfo;
import com.yr.visa.core.model.sys.VisaInfoExample;
import com.yr.visa.util.gen.GenericDao;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VisaInfoMapper extends GenericDao {
    int countByExample(VisaInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(VisaInfo record);

    int insertSelective(VisaInfo record);

    List<VisaInfo> selectByExample(VisaInfoExample example);

    VisaInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") VisaInfo record, @Param("example") VisaInfoExample example);

    int updateByExample(@Param("record") VisaInfo record, @Param("example") VisaInfoExample example);

    int updateByPrimaryKeySelective(VisaInfo record);

    int updateByPrimaryKey(VisaInfo record);
}