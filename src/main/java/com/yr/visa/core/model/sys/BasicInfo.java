package com.yr.visa.core.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yr.visa.util.gen.SuperBean;
import java.util.Date;

public class BasicInfo extends SuperBean {
    private Long id;

    private Long ordId;

    private String familyName;

    private String givenName;

    private String sex;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date birth;

    private String passport;

    private String placeIssure;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date dateIssue;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date dateExpiry;

    private String relationship;

    private String province;

    private String city;

    private String currAddress1;

    private String currAddress2;

    private String currProvince;

    private String currTown;

    private String postalCode;

    private String businesPhone;

    private String mobile;

    private String email;

    private Date addTime;

    private Date modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrdId() {
        return ordId;
    }

    public void setOrdId(Long ordId) {
        this.ordId = ordId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName == null ? null : familyName.trim();
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName == null ? null : givenName.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport == null ? null : passport.trim();
    }

    public String getPlaceIssure() {
        return placeIssure;
    }

    public void setPlaceIssure(String placeIssure) {
        this.placeIssure = placeIssure == null ? null : placeIssure.trim();
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public Date getDateExpiry() {
        return dateExpiry;
    }

    public void setDateExpiry(Date dateExpiry) {
        this.dateExpiry = dateExpiry;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship == null ? null : relationship.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCurrAddress1() {
        return currAddress1;
    }

    public void setCurrAddress1(String currAddress1) {
        this.currAddress1 = currAddress1 == null ? null : currAddress1.trim();
    }

    public String getCurrAddress2() {
        return currAddress2;
    }

    public void setCurrAddress2(String currAddress2) {
        this.currAddress2 = currAddress2 == null ? null : currAddress2.trim();
    }

    public String getCurrProvince() {
        return currProvince;
    }

    public void setCurrProvince(String currProvince) {
        this.currProvince = currProvince == null ? null : currProvince.trim();
    }

    public String getCurrTown() {
        return currTown;
    }

    public void setCurrTown(String currTown) {
        this.currTown = currTown == null ? null : currTown.trim();
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode == null ? null : postalCode.trim();
    }

    public String getBusinesPhone() {
        return businesPhone;
    }

    public void setBusinesPhone(String businesPhone) {
        this.businesPhone = businesPhone == null ? null : businesPhone.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}