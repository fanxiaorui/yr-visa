package com.yr.visa.core.model.sys;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BasicInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BasicInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrdIdIsNull() {
            addCriterion("ord_id is null");
            return (Criteria) this;
        }

        public Criteria andOrdIdIsNotNull() {
            addCriterion("ord_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrdIdEqualTo(Long value) {
            addCriterion("ord_id =", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotEqualTo(Long value) {
            addCriterion("ord_id <>", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdGreaterThan(Long value) {
            addCriterion("ord_id >", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdGreaterThanOrEqualTo(Long value) {
            addCriterion("ord_id >=", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdLessThan(Long value) {
            addCriterion("ord_id <", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdLessThanOrEqualTo(Long value) {
            addCriterion("ord_id <=", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdIn(List<Long> values) {
            addCriterion("ord_id in", values, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotIn(List<Long> values) {
            addCriterion("ord_id not in", values, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdBetween(Long value1, Long value2) {
            addCriterion("ord_id between", value1, value2, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotBetween(Long value1, Long value2) {
            addCriterion("ord_id not between", value1, value2, "ordId");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIsNull() {
            addCriterion("family_name is null");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIsNotNull() {
            addCriterion("family_name is not null");
            return (Criteria) this;
        }

        public Criteria andFamilyNameEqualTo(String value) {
            addCriterion("family_name =", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotEqualTo(String value) {
            addCriterion("family_name <>", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameGreaterThan(String value) {
            addCriterion("family_name >", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameGreaterThanOrEqualTo(String value) {
            addCriterion("family_name >=", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLessThan(String value) {
            addCriterion("family_name <", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLessThanOrEqualTo(String value) {
            addCriterion("family_name <=", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLike(String value) {
            addCriterion("family_name like", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotLike(String value) {
            addCriterion("family_name not like", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIn(List<String> values) {
            addCriterion("family_name in", values, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotIn(List<String> values) {
            addCriterion("family_name not in", values, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameBetween(String value1, String value2) {
            addCriterion("family_name between", value1, value2, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotBetween(String value1, String value2) {
            addCriterion("family_name not between", value1, value2, "familyName");
            return (Criteria) this;
        }

        public Criteria andGivenNameIsNull() {
            addCriterion("given_name is null");
            return (Criteria) this;
        }

        public Criteria andGivenNameIsNotNull() {
            addCriterion("given_name is not null");
            return (Criteria) this;
        }

        public Criteria andGivenNameEqualTo(String value) {
            addCriterion("given_name =", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotEqualTo(String value) {
            addCriterion("given_name <>", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameGreaterThan(String value) {
            addCriterion("given_name >", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameGreaterThanOrEqualTo(String value) {
            addCriterion("given_name >=", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLessThan(String value) {
            addCriterion("given_name <", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLessThanOrEqualTo(String value) {
            addCriterion("given_name <=", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLike(String value) {
            addCriterion("given_name like", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotLike(String value) {
            addCriterion("given_name not like", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameIn(List<String> values) {
            addCriterion("given_name in", values, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotIn(List<String> values) {
            addCriterion("given_name not in", values, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameBetween(String value1, String value2) {
            addCriterion("given_name between", value1, value2, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotBetween(String value1, String value2) {
            addCriterion("given_name not between", value1, value2, "givenName");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("sex like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("sex not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andBirthIsNull() {
            addCriterion("birth is null");
            return (Criteria) this;
        }

        public Criteria andBirthIsNotNull() {
            addCriterion("birth is not null");
            return (Criteria) this;
        }

        public Criteria andBirthEqualTo(Date value) {
            addCriterion("birth =", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotEqualTo(Date value) {
            addCriterion("birth <>", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthGreaterThan(Date value) {
            addCriterion("birth >", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthGreaterThanOrEqualTo(Date value) {
            addCriterion("birth >=", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthLessThan(Date value) {
            addCriterion("birth <", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthLessThanOrEqualTo(Date value) {
            addCriterion("birth <=", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthIn(List<Date> values) {
            addCriterion("birth in", values, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotIn(List<Date> values) {
            addCriterion("birth not in", values, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthBetween(Date value1, Date value2) {
            addCriterion("birth between", value1, value2, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotBetween(Date value1, Date value2) {
            addCriterion("birth not between", value1, value2, "birth");
            return (Criteria) this;
        }

        public Criteria andPassportIsNull() {
            addCriterion("passport is null");
            return (Criteria) this;
        }

        public Criteria andPassportIsNotNull() {
            addCriterion("passport is not null");
            return (Criteria) this;
        }

        public Criteria andPassportEqualTo(String value) {
            addCriterion("passport =", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotEqualTo(String value) {
            addCriterion("passport <>", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportGreaterThan(String value) {
            addCriterion("passport >", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportGreaterThanOrEqualTo(String value) {
            addCriterion("passport >=", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLessThan(String value) {
            addCriterion("passport <", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLessThanOrEqualTo(String value) {
            addCriterion("passport <=", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLike(String value) {
            addCriterion("passport like", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotLike(String value) {
            addCriterion("passport not like", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportIn(List<String> values) {
            addCriterion("passport in", values, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotIn(List<String> values) {
            addCriterion("passport not in", values, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportBetween(String value1, String value2) {
            addCriterion("passport between", value1, value2, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotBetween(String value1, String value2) {
            addCriterion("passport not between", value1, value2, "passport");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureIsNull() {
            addCriterion("place_issure is null");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureIsNotNull() {
            addCriterion("place_issure is not null");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureEqualTo(String value) {
            addCriterion("place_issure =", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureNotEqualTo(String value) {
            addCriterion("place_issure <>", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureGreaterThan(String value) {
            addCriterion("place_issure >", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureGreaterThanOrEqualTo(String value) {
            addCriterion("place_issure >=", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureLessThan(String value) {
            addCriterion("place_issure <", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureLessThanOrEqualTo(String value) {
            addCriterion("place_issure <=", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureLike(String value) {
            addCriterion("place_issure like", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureNotLike(String value) {
            addCriterion("place_issure not like", value, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureIn(List<String> values) {
            addCriterion("place_issure in", values, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureNotIn(List<String> values) {
            addCriterion("place_issure not in", values, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureBetween(String value1, String value2) {
            addCriterion("place_issure between", value1, value2, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andPlaceIssureNotBetween(String value1, String value2) {
            addCriterion("place_issure not between", value1, value2, "placeIssure");
            return (Criteria) this;
        }

        public Criteria andDateIssueIsNull() {
            addCriterion("date_issue is null");
            return (Criteria) this;
        }

        public Criteria andDateIssueIsNotNull() {
            addCriterion("date_issue is not null");
            return (Criteria) this;
        }

        public Criteria andDateIssueEqualTo(Date value) {
            addCriterion("date_issue =", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueNotEqualTo(Date value) {
            addCriterion("date_issue <>", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueGreaterThan(Date value) {
            addCriterion("date_issue >", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueGreaterThanOrEqualTo(Date value) {
            addCriterion("date_issue >=", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueLessThan(Date value) {
            addCriterion("date_issue <", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueLessThanOrEqualTo(Date value) {
            addCriterion("date_issue <=", value, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueIn(List<Date> values) {
            addCriterion("date_issue in", values, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueNotIn(List<Date> values) {
            addCriterion("date_issue not in", values, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueBetween(Date value1, Date value2) {
            addCriterion("date_issue between", value1, value2, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateIssueNotBetween(Date value1, Date value2) {
            addCriterion("date_issue not between", value1, value2, "dateIssue");
            return (Criteria) this;
        }

        public Criteria andDateExpiryIsNull() {
            addCriterion("date_expiry is null");
            return (Criteria) this;
        }

        public Criteria andDateExpiryIsNotNull() {
            addCriterion("date_expiry is not null");
            return (Criteria) this;
        }

        public Criteria andDateExpiryEqualTo(Date value) {
            addCriterion("date_expiry =", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryNotEqualTo(Date value) {
            addCriterion("date_expiry <>", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryGreaterThan(Date value) {
            addCriterion("date_expiry >", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryGreaterThanOrEqualTo(Date value) {
            addCriterion("date_expiry >=", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryLessThan(Date value) {
            addCriterion("date_expiry <", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryLessThanOrEqualTo(Date value) {
            addCriterion("date_expiry <=", value, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryIn(List<Date> values) {
            addCriterion("date_expiry in", values, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryNotIn(List<Date> values) {
            addCriterion("date_expiry not in", values, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryBetween(Date value1, Date value2) {
            addCriterion("date_expiry between", value1, value2, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andDateExpiryNotBetween(Date value1, Date value2) {
            addCriterion("date_expiry not between", value1, value2, "dateExpiry");
            return (Criteria) this;
        }

        public Criteria andRelationshipIsNull() {
            addCriterion("relationship is null");
            return (Criteria) this;
        }

        public Criteria andRelationshipIsNotNull() {
            addCriterion("relationship is not null");
            return (Criteria) this;
        }

        public Criteria andRelationshipEqualTo(String value) {
            addCriterion("relationship =", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotEqualTo(String value) {
            addCriterion("relationship <>", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipGreaterThan(String value) {
            addCriterion("relationship >", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipGreaterThanOrEqualTo(String value) {
            addCriterion("relationship >=", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLessThan(String value) {
            addCriterion("relationship <", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLessThanOrEqualTo(String value) {
            addCriterion("relationship <=", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipLike(String value) {
            addCriterion("relationship like", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotLike(String value) {
            addCriterion("relationship not like", value, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipIn(List<String> values) {
            addCriterion("relationship in", values, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotIn(List<String> values) {
            addCriterion("relationship not in", values, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipBetween(String value1, String value2) {
            addCriterion("relationship between", value1, value2, "relationship");
            return (Criteria) this;
        }

        public Criteria andRelationshipNotBetween(String value1, String value2) {
            addCriterion("relationship not between", value1, value2, "relationship");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1IsNull() {
            addCriterion("curr_address1 is null");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1IsNotNull() {
            addCriterion("curr_address1 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1EqualTo(String value) {
            addCriterion("curr_address1 =", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1NotEqualTo(String value) {
            addCriterion("curr_address1 <>", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1GreaterThan(String value) {
            addCriterion("curr_address1 >", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1GreaterThanOrEqualTo(String value) {
            addCriterion("curr_address1 >=", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1LessThan(String value) {
            addCriterion("curr_address1 <", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1LessThanOrEqualTo(String value) {
            addCriterion("curr_address1 <=", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1Like(String value) {
            addCriterion("curr_address1 like", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1NotLike(String value) {
            addCriterion("curr_address1 not like", value, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1In(List<String> values) {
            addCriterion("curr_address1 in", values, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1NotIn(List<String> values) {
            addCriterion("curr_address1 not in", values, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1Between(String value1, String value2) {
            addCriterion("curr_address1 between", value1, value2, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress1NotBetween(String value1, String value2) {
            addCriterion("curr_address1 not between", value1, value2, "currAddress1");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2IsNull() {
            addCriterion("curr_address2 is null");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2IsNotNull() {
            addCriterion("curr_address2 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2EqualTo(String value) {
            addCriterion("curr_address2 =", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2NotEqualTo(String value) {
            addCriterion("curr_address2 <>", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2GreaterThan(String value) {
            addCriterion("curr_address2 >", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2GreaterThanOrEqualTo(String value) {
            addCriterion("curr_address2 >=", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2LessThan(String value) {
            addCriterion("curr_address2 <", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2LessThanOrEqualTo(String value) {
            addCriterion("curr_address2 <=", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2Like(String value) {
            addCriterion("curr_address2 like", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2NotLike(String value) {
            addCriterion("curr_address2 not like", value, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2In(List<String> values) {
            addCriterion("curr_address2 in", values, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2NotIn(List<String> values) {
            addCriterion("curr_address2 not in", values, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2Between(String value1, String value2) {
            addCriterion("curr_address2 between", value1, value2, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrAddress2NotBetween(String value1, String value2) {
            addCriterion("curr_address2 not between", value1, value2, "currAddress2");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceIsNull() {
            addCriterion("curr_province is null");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceIsNotNull() {
            addCriterion("curr_province is not null");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceEqualTo(String value) {
            addCriterion("curr_province =", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceNotEqualTo(String value) {
            addCriterion("curr_province <>", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceGreaterThan(String value) {
            addCriterion("curr_province >", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("curr_province >=", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceLessThan(String value) {
            addCriterion("curr_province <", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceLessThanOrEqualTo(String value) {
            addCriterion("curr_province <=", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceLike(String value) {
            addCriterion("curr_province like", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceNotLike(String value) {
            addCriterion("curr_province not like", value, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceIn(List<String> values) {
            addCriterion("curr_province in", values, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceNotIn(List<String> values) {
            addCriterion("curr_province not in", values, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceBetween(String value1, String value2) {
            addCriterion("curr_province between", value1, value2, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrProvinceNotBetween(String value1, String value2) {
            addCriterion("curr_province not between", value1, value2, "currProvince");
            return (Criteria) this;
        }

        public Criteria andCurrTownIsNull() {
            addCriterion("curr_town is null");
            return (Criteria) this;
        }

        public Criteria andCurrTownIsNotNull() {
            addCriterion("curr_town is not null");
            return (Criteria) this;
        }

        public Criteria andCurrTownEqualTo(String value) {
            addCriterion("curr_town =", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownNotEqualTo(String value) {
            addCriterion("curr_town <>", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownGreaterThan(String value) {
            addCriterion("curr_town >", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownGreaterThanOrEqualTo(String value) {
            addCriterion("curr_town >=", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownLessThan(String value) {
            addCriterion("curr_town <", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownLessThanOrEqualTo(String value) {
            addCriterion("curr_town <=", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownLike(String value) {
            addCriterion("curr_town like", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownNotLike(String value) {
            addCriterion("curr_town not like", value, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownIn(List<String> values) {
            addCriterion("curr_town in", values, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownNotIn(List<String> values) {
            addCriterion("curr_town not in", values, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownBetween(String value1, String value2) {
            addCriterion("curr_town between", value1, value2, "currTown");
            return (Criteria) this;
        }

        public Criteria andCurrTownNotBetween(String value1, String value2) {
            addCriterion("curr_town not between", value1, value2, "currTown");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIsNull() {
            addCriterion("postal_code is null");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIsNotNull() {
            addCriterion("postal_code is not null");
            return (Criteria) this;
        }

        public Criteria andPostalCodeEqualTo(String value) {
            addCriterion("postal_code =", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotEqualTo(String value) {
            addCriterion("postal_code <>", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeGreaterThan(String value) {
            addCriterion("postal_code >", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeGreaterThanOrEqualTo(String value) {
            addCriterion("postal_code >=", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLessThan(String value) {
            addCriterion("postal_code <", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLessThanOrEqualTo(String value) {
            addCriterion("postal_code <=", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLike(String value) {
            addCriterion("postal_code like", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotLike(String value) {
            addCriterion("postal_code not like", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIn(List<String> values) {
            addCriterion("postal_code in", values, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotIn(List<String> values) {
            addCriterion("postal_code not in", values, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeBetween(String value1, String value2) {
            addCriterion("postal_code between", value1, value2, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotBetween(String value1, String value2) {
            addCriterion("postal_code not between", value1, value2, "postalCode");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneIsNull() {
            addCriterion("busines_phone is null");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneIsNotNull() {
            addCriterion("busines_phone is not null");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneEqualTo(String value) {
            addCriterion("busines_phone =", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneNotEqualTo(String value) {
            addCriterion("busines_phone <>", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneGreaterThan(String value) {
            addCriterion("busines_phone >", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("busines_phone >=", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneLessThan(String value) {
            addCriterion("busines_phone <", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneLessThanOrEqualTo(String value) {
            addCriterion("busines_phone <=", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneLike(String value) {
            addCriterion("busines_phone like", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneNotLike(String value) {
            addCriterion("busines_phone not like", value, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneIn(List<String> values) {
            addCriterion("busines_phone in", values, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneNotIn(List<String> values) {
            addCriterion("busines_phone not in", values, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneBetween(String value1, String value2) {
            addCriterion("busines_phone between", value1, value2, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andBusinesPhoneNotBetween(String value1, String value2) {
            addCriterion("busines_phone not between", value1, value2, "businesPhone");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}