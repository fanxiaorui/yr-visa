package com.yr.visa.core.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yr.visa.util.gen.SuperBean;
import java.util.Date;

public class Ord extends SuperBean {
    private Long id;

    private String familyName;

    private String givenName;

    private String passport;

    private String mobile;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date strokeStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date strokeEndDate;

    private Integer status;

    private Date startDate;

    private Date endDate;

    private String trn;

    private String antUsername;

    private String step;

    private String errorimage;

    private Date addTime;

    private Date modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName == null ? null : familyName.trim();
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName == null ? null : givenName.trim();
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport == null ? null : passport.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Date getStrokeStartDate() {
        return strokeStartDate;
    }

    public void setStrokeStartDate(Date strokeStartDate) {
        this.strokeStartDate = strokeStartDate;
    }

    public Date getStrokeEndDate() {
        return strokeEndDate;
    }

    public void setStrokeEndDate(Date strokeEndDate) {
        this.strokeEndDate = strokeEndDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTrn() {
        return trn;
    }

    public void setTrn(String trn) {
        this.trn = trn == null ? null : trn.trim();
    }

    public String getAntUsername() {
        return antUsername;
    }

    public void setAntUsername(String antUsername) {
        this.antUsername = antUsername == null ? null : antUsername.trim();
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step == null ? null : step.trim();
    }

    public String getErrorimage() {
        return errorimage;
    }

    public void setErrorimage(String errorimage) {
        this.errorimage = errorimage == null ? null : errorimage.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}