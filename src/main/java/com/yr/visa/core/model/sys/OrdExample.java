package com.yr.visa.core.model.sys;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrdExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrdExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIsNull() {
            addCriterion("family_name is null");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIsNotNull() {
            addCriterion("family_name is not null");
            return (Criteria) this;
        }

        public Criteria andFamilyNameEqualTo(String value) {
            addCriterion("family_name =", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotEqualTo(String value) {
            addCriterion("family_name <>", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameGreaterThan(String value) {
            addCriterion("family_name >", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameGreaterThanOrEqualTo(String value) {
            addCriterion("family_name >=", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLessThan(String value) {
            addCriterion("family_name <", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLessThanOrEqualTo(String value) {
            addCriterion("family_name <=", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameLike(String value) {
            addCriterion("family_name like", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotLike(String value) {
            addCriterion("family_name not like", value, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameIn(List<String> values) {
            addCriterion("family_name in", values, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotIn(List<String> values) {
            addCriterion("family_name not in", values, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameBetween(String value1, String value2) {
            addCriterion("family_name between", value1, value2, "familyName");
            return (Criteria) this;
        }

        public Criteria andFamilyNameNotBetween(String value1, String value2) {
            addCriterion("family_name not between", value1, value2, "familyName");
            return (Criteria) this;
        }

        public Criteria andGivenNameIsNull() {
            addCriterion("given_name is null");
            return (Criteria) this;
        }

        public Criteria andGivenNameIsNotNull() {
            addCriterion("given_name is not null");
            return (Criteria) this;
        }

        public Criteria andGivenNameEqualTo(String value) {
            addCriterion("given_name =", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotEqualTo(String value) {
            addCriterion("given_name <>", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameGreaterThan(String value) {
            addCriterion("given_name >", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameGreaterThanOrEqualTo(String value) {
            addCriterion("given_name >=", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLessThan(String value) {
            addCriterion("given_name <", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLessThanOrEqualTo(String value) {
            addCriterion("given_name <=", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameLike(String value) {
            addCriterion("given_name like", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotLike(String value) {
            addCriterion("given_name not like", value, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameIn(List<String> values) {
            addCriterion("given_name in", values, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotIn(List<String> values) {
            addCriterion("given_name not in", values, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameBetween(String value1, String value2) {
            addCriterion("given_name between", value1, value2, "givenName");
            return (Criteria) this;
        }

        public Criteria andGivenNameNotBetween(String value1, String value2) {
            addCriterion("given_name not between", value1, value2, "givenName");
            return (Criteria) this;
        }

        public Criteria andPassportIsNull() {
            addCriterion("passport is null");
            return (Criteria) this;
        }

        public Criteria andPassportIsNotNull() {
            addCriterion("passport is not null");
            return (Criteria) this;
        }

        public Criteria andPassportEqualTo(String value) {
            addCriterion("passport =", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotEqualTo(String value) {
            addCriterion("passport <>", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportGreaterThan(String value) {
            addCriterion("passport >", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportGreaterThanOrEqualTo(String value) {
            addCriterion("passport >=", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLessThan(String value) {
            addCriterion("passport <", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLessThanOrEqualTo(String value) {
            addCriterion("passport <=", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportLike(String value) {
            addCriterion("passport like", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotLike(String value) {
            addCriterion("passport not like", value, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportIn(List<String> values) {
            addCriterion("passport in", values, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotIn(List<String> values) {
            addCriterion("passport not in", values, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportBetween(String value1, String value2) {
            addCriterion("passport between", value1, value2, "passport");
            return (Criteria) this;
        }

        public Criteria andPassportNotBetween(String value1, String value2) {
            addCriterion("passport not between", value1, value2, "passport");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateIsNull() {
            addCriterion("stroke_start_date is null");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateIsNotNull() {
            addCriterion("stroke_start_date is not null");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateEqualTo(Date value) {
            addCriterion("stroke_start_date =", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateNotEqualTo(Date value) {
            addCriterion("stroke_start_date <>", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateGreaterThan(Date value) {
            addCriterion("stroke_start_date >", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("stroke_start_date >=", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateLessThan(Date value) {
            addCriterion("stroke_start_date <", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateLessThanOrEqualTo(Date value) {
            addCriterion("stroke_start_date <=", value, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateIn(List<Date> values) {
            addCriterion("stroke_start_date in", values, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateNotIn(List<Date> values) {
            addCriterion("stroke_start_date not in", values, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateBetween(Date value1, Date value2) {
            addCriterion("stroke_start_date between", value1, value2, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeStartDateNotBetween(Date value1, Date value2) {
            addCriterion("stroke_start_date not between", value1, value2, "strokeStartDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateIsNull() {
            addCriterion("stroke_end_date is null");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateIsNotNull() {
            addCriterion("stroke_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateEqualTo(Date value) {
            addCriterion("stroke_end_date =", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateNotEqualTo(Date value) {
            addCriterion("stroke_end_date <>", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateGreaterThan(Date value) {
            addCriterion("stroke_end_date >", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("stroke_end_date >=", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateLessThan(Date value) {
            addCriterion("stroke_end_date <", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateLessThanOrEqualTo(Date value) {
            addCriterion("stroke_end_date <=", value, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateIn(List<Date> values) {
            addCriterion("stroke_end_date in", values, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateNotIn(List<Date> values) {
            addCriterion("stroke_end_date not in", values, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateBetween(Date value1, Date value2) {
            addCriterion("stroke_end_date between", value1, value2, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStrokeEndDateNotBetween(Date value1, Date value2) {
            addCriterion("stroke_end_date not between", value1, value2, "strokeEndDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNull() {
            addCriterion("start_date is null");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNotNull() {
            addCriterion("start_date is not null");
            return (Criteria) this;
        }

        public Criteria andStartDateEqualTo(Date value) {
            addCriterion("start_date =", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotEqualTo(Date value) {
            addCriterion("start_date <>", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThan(Date value) {
            addCriterion("start_date >", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("start_date >=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThan(Date value) {
            addCriterion("start_date <", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThanOrEqualTo(Date value) {
            addCriterion("start_date <=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateIn(List<Date> values) {
            addCriterion("start_date in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotIn(List<Date> values) {
            addCriterion("start_date not in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateBetween(Date value1, Date value2) {
            addCriterion("start_date between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotBetween(Date value1, Date value2) {
            addCriterion("start_date not between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNull() {
            addCriterion("end_date is null");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNotNull() {
            addCriterion("end_date is not null");
            return (Criteria) this;
        }

        public Criteria andEndDateEqualTo(Date value) {
            addCriterion("end_date =", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotEqualTo(Date value) {
            addCriterion("end_date <>", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThan(Date value) {
            addCriterion("end_date >", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("end_date >=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThan(Date value) {
            addCriterion("end_date <", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThanOrEqualTo(Date value) {
            addCriterion("end_date <=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIn(List<Date> values) {
            addCriterion("end_date in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotIn(List<Date> values) {
            addCriterion("end_date not in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateBetween(Date value1, Date value2) {
            addCriterion("end_date between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotBetween(Date value1, Date value2) {
            addCriterion("end_date not between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andTrnIsNull() {
            addCriterion("trn is null");
            return (Criteria) this;
        }

        public Criteria andTrnIsNotNull() {
            addCriterion("trn is not null");
            return (Criteria) this;
        }

        public Criteria andTrnEqualTo(String value) {
            addCriterion("trn =", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnNotEqualTo(String value) {
            addCriterion("trn <>", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnGreaterThan(String value) {
            addCriterion("trn >", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnGreaterThanOrEqualTo(String value) {
            addCriterion("trn >=", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnLessThan(String value) {
            addCriterion("trn <", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnLessThanOrEqualTo(String value) {
            addCriterion("trn <=", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnLike(String value) {
            addCriterion("trn like", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnNotLike(String value) {
            addCriterion("trn not like", value, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnIn(List<String> values) {
            addCriterion("trn in", values, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnNotIn(List<String> values) {
            addCriterion("trn not in", values, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnBetween(String value1, String value2) {
            addCriterion("trn between", value1, value2, "trn");
            return (Criteria) this;
        }

        public Criteria andTrnNotBetween(String value1, String value2) {
            addCriterion("trn not between", value1, value2, "trn");
            return (Criteria) this;
        }

        public Criteria andAntUsernameIsNull() {
            addCriterion("ant_username is null");
            return (Criteria) this;
        }

        public Criteria andAntUsernameIsNotNull() {
            addCriterion("ant_username is not null");
            return (Criteria) this;
        }

        public Criteria andAntUsernameEqualTo(String value) {
            addCriterion("ant_username =", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameNotEqualTo(String value) {
            addCriterion("ant_username <>", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameGreaterThan(String value) {
            addCriterion("ant_username >", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("ant_username >=", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameLessThan(String value) {
            addCriterion("ant_username <", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameLessThanOrEqualTo(String value) {
            addCriterion("ant_username <=", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameLike(String value) {
            addCriterion("ant_username like", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameNotLike(String value) {
            addCriterion("ant_username not like", value, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameIn(List<String> values) {
            addCriterion("ant_username in", values, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameNotIn(List<String> values) {
            addCriterion("ant_username not in", values, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameBetween(String value1, String value2) {
            addCriterion("ant_username between", value1, value2, "antUsername");
            return (Criteria) this;
        }

        public Criteria andAntUsernameNotBetween(String value1, String value2) {
            addCriterion("ant_username not between", value1, value2, "antUsername");
            return (Criteria) this;
        }

        public Criteria andStepIsNull() {
            addCriterion("step is null");
            return (Criteria) this;
        }

        public Criteria andStepIsNotNull() {
            addCriterion("step is not null");
            return (Criteria) this;
        }

        public Criteria andStepEqualTo(String value) {
            addCriterion("step =", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepNotEqualTo(String value) {
            addCriterion("step <>", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepGreaterThan(String value) {
            addCriterion("step >", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepGreaterThanOrEqualTo(String value) {
            addCriterion("step >=", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepLessThan(String value) {
            addCriterion("step <", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepLessThanOrEqualTo(String value) {
            addCriterion("step <=", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepLike(String value) {
            addCriterion("step like", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepNotLike(String value) {
            addCriterion("step not like", value, "step");
            return (Criteria) this;
        }

        public Criteria andStepIn(List<String> values) {
            addCriterion("step in", values, "step");
            return (Criteria) this;
        }

        public Criteria andStepNotIn(List<String> values) {
            addCriterion("step not in", values, "step");
            return (Criteria) this;
        }

        public Criteria andStepBetween(String value1, String value2) {
            addCriterion("step between", value1, value2, "step");
            return (Criteria) this;
        }

        public Criteria andStepNotBetween(String value1, String value2) {
            addCriterion("step not between", value1, value2, "step");
            return (Criteria) this;
        }

        public Criteria andErrorimageIsNull() {
            addCriterion("errorImage is null");
            return (Criteria) this;
        }

        public Criteria andErrorimageIsNotNull() {
            addCriterion("errorImage is not null");
            return (Criteria) this;
        }

        public Criteria andErrorimageEqualTo(String value) {
            addCriterion("errorImage =", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageNotEqualTo(String value) {
            addCriterion("errorImage <>", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageGreaterThan(String value) {
            addCriterion("errorImage >", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageGreaterThanOrEqualTo(String value) {
            addCriterion("errorImage >=", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageLessThan(String value) {
            addCriterion("errorImage <", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageLessThanOrEqualTo(String value) {
            addCriterion("errorImage <=", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageLike(String value) {
            addCriterion("errorImage like", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageNotLike(String value) {
            addCriterion("errorImage not like", value, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageIn(List<String> values) {
            addCriterion("errorImage in", values, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageNotIn(List<String> values) {
            addCriterion("errorImage not in", values, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageBetween(String value1, String value2) {
            addCriterion("errorImage between", value1, value2, "errorimage");
            return (Criteria) this;
        }

        public Criteria andErrorimageNotBetween(String value1, String value2) {
            addCriterion("errorImage not between", value1, value2, "errorimage");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}