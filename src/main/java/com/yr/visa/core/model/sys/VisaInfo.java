package com.yr.visa.core.model.sys;

import com.yr.visa.util.gen.SuperBean;
import java.util.Date;

public class VisaInfo extends SuperBean {
    private Long id;

    private Long ordId;

    private String fundingStayInfo;

    private String vistitReason;

    private String hastoOtherCountry;

    private String hastoOtherCountryDetails;

    private String hasRefuse;

    private String hasRefuseDetails;

    private Date addTime;

    private Date modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrdId() {
        return ordId;
    }

    public void setOrdId(Long ordId) {
        this.ordId = ordId;
    }

    public String getFundingStayInfo() {
        return fundingStayInfo;
    }

    public void setFundingStayInfo(String fundingStayInfo) {
        this.fundingStayInfo = fundingStayInfo == null ? null : fundingStayInfo.trim();
    }

    public String getVistitReason() {
        return vistitReason;
    }

    public void setVistitReason(String vistitReason) {
        this.vistitReason = vistitReason == null ? null : vistitReason.trim();
    }

    public String getHastoOtherCountry() {
        return hastoOtherCountry;
    }

    public void setHastoOtherCountry(String hastoOtherCountry) {
        this.hastoOtherCountry = hastoOtherCountry == null ? null : hastoOtherCountry.trim();
    }

    public String getHastoOtherCountryDetails() {
        return hastoOtherCountryDetails;
    }

    public void setHastoOtherCountryDetails(String hastoOtherCountryDetails) {
        this.hastoOtherCountryDetails = hastoOtherCountryDetails == null ? null : hastoOtherCountryDetails.trim();
    }

    public String getHasRefuse() {
        return hasRefuse;
    }

    public void setHasRefuse(String hasRefuse) {
        this.hasRefuse = hasRefuse == null ? null : hasRefuse.trim();
    }

    public String getHasRefuseDetails() {
        return hasRefuseDetails;
    }

    public void setHasRefuseDetails(String hasRefuseDetails) {
        this.hasRefuseDetails = hasRefuseDetails == null ? null : hasRefuseDetails.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}