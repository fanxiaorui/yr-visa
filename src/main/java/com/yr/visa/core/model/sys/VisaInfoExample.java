package com.yr.visa.core.model.sys;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VisaInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VisaInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrdIdIsNull() {
            addCriterion("ord_id is null");
            return (Criteria) this;
        }

        public Criteria andOrdIdIsNotNull() {
            addCriterion("ord_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrdIdEqualTo(Long value) {
            addCriterion("ord_id =", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotEqualTo(Long value) {
            addCriterion("ord_id <>", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdGreaterThan(Long value) {
            addCriterion("ord_id >", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdGreaterThanOrEqualTo(Long value) {
            addCriterion("ord_id >=", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdLessThan(Long value) {
            addCriterion("ord_id <", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdLessThanOrEqualTo(Long value) {
            addCriterion("ord_id <=", value, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdIn(List<Long> values) {
            addCriterion("ord_id in", values, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotIn(List<Long> values) {
            addCriterion("ord_id not in", values, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdBetween(Long value1, Long value2) {
            addCriterion("ord_id between", value1, value2, "ordId");
            return (Criteria) this;
        }

        public Criteria andOrdIdNotBetween(Long value1, Long value2) {
            addCriterion("ord_id not between", value1, value2, "ordId");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoIsNull() {
            addCriterion("funding_stay_info is null");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoIsNotNull() {
            addCriterion("funding_stay_info is not null");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoEqualTo(String value) {
            addCriterion("funding_stay_info =", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoNotEqualTo(String value) {
            addCriterion("funding_stay_info <>", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoGreaterThan(String value) {
            addCriterion("funding_stay_info >", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoGreaterThanOrEqualTo(String value) {
            addCriterion("funding_stay_info >=", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoLessThan(String value) {
            addCriterion("funding_stay_info <", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoLessThanOrEqualTo(String value) {
            addCriterion("funding_stay_info <=", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoLike(String value) {
            addCriterion("funding_stay_info like", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoNotLike(String value) {
            addCriterion("funding_stay_info not like", value, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoIn(List<String> values) {
            addCriterion("funding_stay_info in", values, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoNotIn(List<String> values) {
            addCriterion("funding_stay_info not in", values, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoBetween(String value1, String value2) {
            addCriterion("funding_stay_info between", value1, value2, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andFundingStayInfoNotBetween(String value1, String value2) {
            addCriterion("funding_stay_info not between", value1, value2, "fundingStayInfo");
            return (Criteria) this;
        }

        public Criteria andVistitReasonIsNull() {
            addCriterion("vistit_reason is null");
            return (Criteria) this;
        }

        public Criteria andVistitReasonIsNotNull() {
            addCriterion("vistit_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVistitReasonEqualTo(String value) {
            addCriterion("vistit_reason =", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonNotEqualTo(String value) {
            addCriterion("vistit_reason <>", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonGreaterThan(String value) {
            addCriterion("vistit_reason >", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonGreaterThanOrEqualTo(String value) {
            addCriterion("vistit_reason >=", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonLessThan(String value) {
            addCriterion("vistit_reason <", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonLessThanOrEqualTo(String value) {
            addCriterion("vistit_reason <=", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonLike(String value) {
            addCriterion("vistit_reason like", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonNotLike(String value) {
            addCriterion("vistit_reason not like", value, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonIn(List<String> values) {
            addCriterion("vistit_reason in", values, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonNotIn(List<String> values) {
            addCriterion("vistit_reason not in", values, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonBetween(String value1, String value2) {
            addCriterion("vistit_reason between", value1, value2, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andVistitReasonNotBetween(String value1, String value2) {
            addCriterion("vistit_reason not between", value1, value2, "vistitReason");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryIsNull() {
            addCriterion("hasto_other_country is null");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryIsNotNull() {
            addCriterion("hasto_other_country is not null");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryEqualTo(String value) {
            addCriterion("hasto_other_country =", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryNotEqualTo(String value) {
            addCriterion("hasto_other_country <>", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryGreaterThan(String value) {
            addCriterion("hasto_other_country >", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryGreaterThanOrEqualTo(String value) {
            addCriterion("hasto_other_country >=", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryLessThan(String value) {
            addCriterion("hasto_other_country <", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryLessThanOrEqualTo(String value) {
            addCriterion("hasto_other_country <=", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryLike(String value) {
            addCriterion("hasto_other_country like", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryNotLike(String value) {
            addCriterion("hasto_other_country not like", value, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryIn(List<String> values) {
            addCriterion("hasto_other_country in", values, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryNotIn(List<String> values) {
            addCriterion("hasto_other_country not in", values, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryBetween(String value1, String value2) {
            addCriterion("hasto_other_country between", value1, value2, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryNotBetween(String value1, String value2) {
            addCriterion("hasto_other_country not between", value1, value2, "hastoOtherCountry");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsIsNull() {
            addCriterion("hasto_other_country_details is null");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsIsNotNull() {
            addCriterion("hasto_other_country_details is not null");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsEqualTo(String value) {
            addCriterion("hasto_other_country_details =", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsNotEqualTo(String value) {
            addCriterion("hasto_other_country_details <>", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsGreaterThan(String value) {
            addCriterion("hasto_other_country_details >", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsGreaterThanOrEqualTo(String value) {
            addCriterion("hasto_other_country_details >=", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsLessThan(String value) {
            addCriterion("hasto_other_country_details <", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsLessThanOrEqualTo(String value) {
            addCriterion("hasto_other_country_details <=", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsLike(String value) {
            addCriterion("hasto_other_country_details like", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsNotLike(String value) {
            addCriterion("hasto_other_country_details not like", value, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsIn(List<String> values) {
            addCriterion("hasto_other_country_details in", values, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsNotIn(List<String> values) {
            addCriterion("hasto_other_country_details not in", values, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsBetween(String value1, String value2) {
            addCriterion("hasto_other_country_details between", value1, value2, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHastoOtherCountryDetailsNotBetween(String value1, String value2) {
            addCriterion("hasto_other_country_details not between", value1, value2, "hastoOtherCountryDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseIsNull() {
            addCriterion("has_refuse is null");
            return (Criteria) this;
        }

        public Criteria andHasRefuseIsNotNull() {
            addCriterion("has_refuse is not null");
            return (Criteria) this;
        }

        public Criteria andHasRefuseEqualTo(String value) {
            addCriterion("has_refuse =", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseNotEqualTo(String value) {
            addCriterion("has_refuse <>", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseGreaterThan(String value) {
            addCriterion("has_refuse >", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseGreaterThanOrEqualTo(String value) {
            addCriterion("has_refuse >=", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseLessThan(String value) {
            addCriterion("has_refuse <", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseLessThanOrEqualTo(String value) {
            addCriterion("has_refuse <=", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseLike(String value) {
            addCriterion("has_refuse like", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseNotLike(String value) {
            addCriterion("has_refuse not like", value, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseIn(List<String> values) {
            addCriterion("has_refuse in", values, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseNotIn(List<String> values) {
            addCriterion("has_refuse not in", values, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseBetween(String value1, String value2) {
            addCriterion("has_refuse between", value1, value2, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseNotBetween(String value1, String value2) {
            addCriterion("has_refuse not between", value1, value2, "hasRefuse");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsIsNull() {
            addCriterion("has_refuse_details is null");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsIsNotNull() {
            addCriterion("has_refuse_details is not null");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsEqualTo(String value) {
            addCriterion("has_refuse_details =", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsNotEqualTo(String value) {
            addCriterion("has_refuse_details <>", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsGreaterThan(String value) {
            addCriterion("has_refuse_details >", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsGreaterThanOrEqualTo(String value) {
            addCriterion("has_refuse_details >=", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsLessThan(String value) {
            addCriterion("has_refuse_details <", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsLessThanOrEqualTo(String value) {
            addCriterion("has_refuse_details <=", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsLike(String value) {
            addCriterion("has_refuse_details like", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsNotLike(String value) {
            addCriterion("has_refuse_details not like", value, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsIn(List<String> values) {
            addCriterion("has_refuse_details in", values, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsNotIn(List<String> values) {
            addCriterion("has_refuse_details not in", values, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsBetween(String value1, String value2) {
            addCriterion("has_refuse_details between", value1, value2, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andHasRefuseDetailsNotBetween(String value1, String value2) {
            addCriterion("has_refuse_details not between", value1, value2, "hasRefuseDetails");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}