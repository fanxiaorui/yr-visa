package com.yr.visa.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.BasicInfoMapper;
import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.BasicInfoExample;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;

@Service
public class BasicInfoServices extends GenericServiceImpl<BasicInfo, Long>{

	@Autowired
	BasicInfoMapper basicInfoMapper;

	@Override
	public GenericDao<BasicInfo, Long> getDao() {
		// TODO Auto-generated method stub
		return basicInfoMapper;
	}
	
	
	public BasicInfo selectByOrdId(Long id){
		BasicInfoExample e = new BasicInfoExample();
		e.createCriteria().andOrdIdEqualTo(id);
		List<BasicInfo> bi = this.basicInfoMapper.selectByExample(e);
		
		return (bi==null||bi.isEmpty())?null:bi.get(0);
	}
	
}
