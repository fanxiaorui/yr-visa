package com.yr.visa.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.EmployInfoMapper;
import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.EmployInfoExample;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;

@Service
public class EmployInfoServices extends GenericServiceImpl<EmployInfo, Long> {

	@Autowired
	EmployInfoMapper employInfoMapper;
	@Override
	public GenericDao<EmployInfo, Long> getDao() {
		// TODO Auto-generated method stub
		return employInfoMapper;
	}
	
	public EmployInfo selectByOrdId(Long id){
		EmployInfoExample e = new EmployInfoExample();
		e.createCriteria().andOrdIdEqualTo(id);
		List<EmployInfo> bi = this.employInfoMapper.selectByExample(e);
		
		return (bi==null||bi.isEmpty())?null:bi.get(0);
	}

}
