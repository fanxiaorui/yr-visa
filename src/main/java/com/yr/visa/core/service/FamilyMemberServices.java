package com.yr.visa.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.FamilyMemberMapper;
import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.FamilyMemberExample;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;

@Service
public class FamilyMemberServices extends GenericServiceImpl<FamilyMember, Long> {

	@Autowired
	FamilyMemberMapper familyMemberMapper;
	@Override
	public GenericDao<FamilyMember, Long> getDao() {
		// TODO Auto-generated method stub
		return familyMemberMapper;
	}
	public List<FamilyMember> selectByOrdId(Long id){
		FamilyMemberExample e = new FamilyMemberExample();
		e.createCriteria().andOrdIdEqualTo(id);
		List<FamilyMember> bi = this.familyMemberMapper.selectByExample(e);
		
		return bi;
	}
}
