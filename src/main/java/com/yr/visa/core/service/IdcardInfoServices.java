package com.yr.visa.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.IdcardInfoMapper;
import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.IdcardInfoExample;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;

@Service
public class IdcardInfoServices extends GenericServiceImpl<IdcardInfo, Long> {

	@Autowired
	IdcardInfoMapper idcardInfoMapper;
	@Override
	public GenericDao<IdcardInfo, Long> getDao() {
		// TODO Auto-generated method stub
		return idcardInfoMapper;
	}
	
	public IdcardInfo selectByOrdId(Long id){
		IdcardInfoExample e = new IdcardInfoExample();
		e.createCriteria().andOrdIdEqualTo(id);
		List<IdcardInfo> bi = this.idcardInfoMapper.selectByExample(e);
		
		return (bi==null||bi.isEmpty())?null:bi.get(0);
	}

}
