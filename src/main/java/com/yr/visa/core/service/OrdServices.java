package com.yr.visa.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yr.visa.core.mapper.sys.OrdMapper;
import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.Ord;
import com.yr.visa.core.model.sys.OrdExample;
import com.yr.visa.core.model.sys.VisaInfo;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;
import com.yr.visa.vo.CoreRequest;

@Service
public class OrdServices extends GenericServiceImpl<Ord, Long>{

	@Autowired
	OrdMapper ordMapper;
	@Autowired
	BasicInfoServices basicInfoServices;
	@Autowired
	IdcardInfoServices idcardInfoServices;
	@Autowired
	EmployInfoServices employInfoServices;
	@Autowired
	VisaInfoServices visaInfoServices;
	@Autowired
	FamilyMemberServices familyMemberServices;
	@Override
	public GenericDao<Ord, Long> getDao() {
		// TODO Auto-generated method stub
		return ordMapper;
	}
	
	
	public Page<Ord> selectOrds(String isTemp , int offset, int limit){
		PageHelper.startPage(offset, limit);
		OrdExample e = new OrdExample();
		if("1".equals(isTemp)){
			e.createCriteria().andStatusEqualTo(-1);
		}else{
			e.createCriteria().andStatusGreaterThan(-1);
		}
		return (Page<Ord>)this.ordMapper.selectByExample(e);
	}
	
	public CoreRequest getOrdDetails(Long id){
		Ord o =this.selectByPrimaryKey(id);
		if(o == null){
			return null;
		}
		CoreRequest c = new CoreRequest();
		c.setOrd(o);
		c.setBinfo(this.basicInfoServices.selectByOrdId(id));
		c.setEmployInfo(this.employInfoServices.selectByOrdId(id));
		c.setFamilyMembers(this.familyMemberServices.selectByOrdId(id));
		c.setIdcard(this.idcardInfoServices.selectByOrdId(id));
		c.setVisInfo(this.visaInfoServices.selectByOrdId(id));
		return c;
	}
	
	
	@Transactional
	public void saveCoreRequest(CoreRequest request){
		Ord o = request.getOrd();
		BasicInfo binfo =request.getBinfo();
		if(o == null){
			o = new Ord();
			o.setAddTime(new Date());
		}
		if(binfo != null){
			o.setFamilyName(binfo.getFamilyName());
			o.setGivenName(binfo.getGivenName());
			o.setPassport(binfo.getPassport());
			o.setMobile(binfo.getMobile());
		}
		o.setStatus(request.getSaveStatus()==0?-1:1);
		o.setModifyTime(new Date());
		if(o.getId() == null){
			o.setAddTime(new Date());
			this.insert(o);
		}else{
			this.updateBySelective(o);
		}
		// basic info 
		
		if(binfo != null){
			binfo.setModifyTime(new Date());
			binfo.setOrdId(o.getId());
			if(binfo.getId() == null){
				binfo.setAddTime(new Date());
				basicInfoServices.insert(binfo);
			}else{
				basicInfoServices.updateBySelective(binfo);
			}
		}
		
		
		//IdcardInfo
		IdcardInfo idcard = request.getIdcard();
		if(idcard != null){
			idcard.setModifyTime(new Date());
			idcard.setOrdId(o.getId());
			if(idcard.getId() == null){
				idcard.setAddTime(new Date());
				idcardInfoServices.insert(idcard);
			}else{
				idcardInfoServices.updateBySelective(idcard);
			}
		}
		
		//EmployInfo
		EmployInfo employInfo =request.getEmployInfo();
		if(employInfo != null){
			employInfo.setModifyTime(new Date());
			employInfo.setOrdId(o.getId());
			if(employInfo.getId() == null){
				employInfo.setAddTime(new Date());
				employInfoServices.insert(employInfo);
			}else{
				employInfoServices.updateBySelective(employInfo);
			}
			
		}
		
		//visInfo
		VisaInfo visInfo = request.getVisInfo();
		if(visInfo != null){
			visInfo.setModifyTime(new Date());
			visInfo.setOrdId(o.getId());
			if(visInfo.getId() == null){
				visInfo.setAddTime(new Date());
				visaInfoServices.insert(visInfo);
			}else{
				visaInfoServices.updateBySelective(visInfo);
			}
		}
		
		
		List<FamilyMember> familyMembers = request.getFamilyMembers();
		if(familyMembers!= null){
			for (FamilyMember familyMember : familyMembers) {
				if(familyMember.getId() == null){
					familyMember.setAddTime(new Date());
					familyMember.setModifyTime(new Date());
					familyMember.setOrdId(o.getId());
					familyMemberServices.insert(familyMember);
				}else{
					familyMember.setModifyTime(new Date());
					familyMember.setOrdId(o.getId());
					familyMemberServices.updateBySelective(familyMember);
				}
			}
		}
	}

}
