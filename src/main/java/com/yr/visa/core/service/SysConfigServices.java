package com.yr.visa.core.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.ConfigMapper;
import com.yr.visa.core.model.sys.Config;
import com.yr.visa.core.model.sys.ConfigExample;
import com.yr.visa.util.StringUtil;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;


@Service
public class SysConfigServices extends GenericServiceImpl<Config, Long>{

	@Autowired
	ConfigMapper configMapper;
	
	@Override
	public GenericDao<Config, Long> getDao() {
		return configMapper;
	}
	
	public List<Config> getAll(){
		ConfigExample e = new ConfigExample();
		e.createCriteria().andAddTimeIsNotNull();
		return configMapper.selectByExample(e);
	}
	
	public Config selectOne(ConfigExample e){
		List<Config> configs =configMapper.selectByExample(e);
		return (configs==null||configs.isEmpty())?null:configs.get(0);
	}
	
	public void updateAll(Map<String, String> configs){
		if(configs == null){
			return;
		}
		configs.forEach((k,v) -> {
			if(StringUtil.isBlank(k) || StringUtil.isBlank(v) ){
				return;
			}
			ConfigExample e = new ConfigExample();
			e.createCriteria().andCkeyEqualTo(k);
			Config c =selectOne(e);
			if(c != null){
				c.setCvalue(v);
				c.setModifyTime(new Date());
				this.updateBySelective(c);
			}
		});
		
	}

}
