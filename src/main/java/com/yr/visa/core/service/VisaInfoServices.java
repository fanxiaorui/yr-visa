package com.yr.visa.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yr.visa.core.mapper.sys.VisaInfoMapper;
import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.IdcardInfoExample;
import com.yr.visa.core.model.sys.VisaInfo;
import com.yr.visa.core.model.sys.VisaInfoExample;
import com.yr.visa.util.gen.GenericDao;
import com.yr.visa.util.gen.GenericServiceImpl;

@Service
public class VisaInfoServices extends GenericServiceImpl<VisaInfo, Long>{

	@Autowired
	VisaInfoMapper visaInfoMapper;
	@Override
	public GenericDao<VisaInfo, Long> getDao() {
		// TODO Auto-generated method stub
		return visaInfoMapper;
	}
	public VisaInfo selectByOrdId(Long id){
		VisaInfoExample e = new VisaInfoExample();
		e.createCriteria().andOrdIdEqualTo(id);
		List<VisaInfo> bi = this.visaInfoMapper.selectByExample(e);
		
		return (bi==null||bi.isEmpty())?null:bi.get(0);
	}
}
