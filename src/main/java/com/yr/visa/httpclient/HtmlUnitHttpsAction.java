package com.yr.visa.httpclient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

public class HtmlUnitHttpsAction {

	
	public static HtmlPage doRequest(WebClient webClient,Request r){
		
		try {
			WebRequest	webRequest = new WebRequest(new URL(r.getUrl()));
			if(r.getMethod().equals(HttpConstant.Method.GET)){
				webRequest.setHttpMethod(HttpMethod.GET);
			}else if (r.getMethod().equals(HttpConstant.Method.POST)){
				webRequest.setHttpMethod(HttpMethod.POST);
				if(r.getFormParams() !=null){
					webRequest.setRequestParameters(new ArrayList<NameValuePair>());
					for(org.apache.http.NameValuePair param : r.getFormParams() ){
						webRequest.getRequestParameters().add(
								new NameValuePair(param.getName(), param.getValue()));
					}
				}
			}
			
			return webClient.getPage(webRequest);
		} catch ( FailingHttpStatusCodeException | IOException e) {
			
			e.printStackTrace();
			return null;
		}
		
		
		
		
	}
}
