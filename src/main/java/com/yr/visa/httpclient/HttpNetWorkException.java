package com.yr.visa.httpclient;

/**
 * 用户标识网络异常
 * @author fanrui
 *
 */
public class HttpNetWorkException extends RuntimeException {

	public HttpNetWorkException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HttpNetWorkException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public HttpNetWorkException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public HttpNetWorkException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public HttpNetWorkException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
