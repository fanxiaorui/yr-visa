package com.yr.visa.httpclient;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.HttpResponse;

public class Response {

	private HttpResponse httpResponse;

	private Request request;

	private String url;

	private int statusCode;

	private String rawText;

	private File file;

	private String extendsions;
	
	private boolean ok;
	
	public boolean isOk(){
		return statusCode==200&&(StringUtils.isNotBlank(rawText)||file!=null);
	}

	public String getExtendsions() {
		return extendsions;
	}

	public void setExtendsions(String extendsions) {
		this.extendsions = extendsions;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public HttpResponse getHttpResponse() {
		return httpResponse;
	}

	public void setHttpResponse(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getRawText() {
		return rawText;
	}

	public void setRawText(String rawText) {
		this.rawText = rawText;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
