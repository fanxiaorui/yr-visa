package com.yr.visa.httpclient;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;

public class Site {

	
	private String userAgent="Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.04";
	
	private boolean useGzip=false;
	
	private int retryTimes=3;
	
	/**
	 * 默认站点
	 */
	private String domain="www.btsales.com";
	
	private SSLConnectionSocketFactory sslContextFactory;
	
	private boolean isEnableProxy=false;
	
	
	



	public boolean isEnableProxy() {
		return isEnableProxy;
	}

	public void setEnableProxy(boolean isEnableProxy) {
		this.isEnableProxy = isEnableProxy;
	}

	public SSLConnectionSocketFactory getSslContextFactory() {
		return sslContextFactory;
	}

	public void setSslContextFactory(SSLConnectionSocketFactory sslContextFactory) {
		this.sslContextFactory = sslContextFactory;
	}

	public Site(){}

	public Site(String domain) {
		super();
		this.domain = domain;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public boolean isUseGzip() {
		return useGzip;
	}

	public void setUseGzip(boolean useGzip) {
		this.useGzip = useGzip;
	}

	public int getRetryTimes() {
		return retryTimes;
	}

	public void setRetryTimes(int retryTimes) {
		this.retryTimes = retryTimes;
	}
	
	
}
