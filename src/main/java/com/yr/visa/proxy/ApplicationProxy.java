package com.yr.visa.proxy;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.VisaInfo;
import com.yr.visa.proxy.ctx.Brower;
import com.yr.visa.proxy.ctx.WcAccessException;
import com.yr.visa.util.DateUtils;
import com.yr.visa.util.PlaceholderUtils;
import com.yr.visa.vo.CoreRepsonse;
import com.yr.visa.vo.CoreRequest;

@Component
public class ApplicationProxy {
	private static Logger log = LoggerFactory.getLogger(ApplicationProxy.class);
	public CoreRepsonse coreRequest(Brower b, CoreRequest r) {
		CoreRepsonse response = new CoreRepsonse();
		response.setTaskId(r.getTaskId());
		try {
			phstep1(b, r,response);
			phstep2(b, r,response);
			phstep3(b, r,response);
			phstep4(b, r,response);
			phstep5(b, r,response);
			phstep6(b, r,response);
			phstep7(b, r,response);
			phstep8(b, r,response);
			phstep9(b, r,response);
			phstep10(b, r,response);
			phstep12(b, r,response);
			phstep13(b, r,response);
			phstep16(b, r,response);
			phstep17(b, r,response);
			phstep18(b, r,response);
			phstep20(b, r,response);
			phstep21(b, r,response);
			confirmPage(b, r,response);
			response.setStatus("1");
		} catch (TimeoutException e) {
			log.error("",e);
			response.setErrorImage(b.printScreen(null, "error-"));
		} finally{
			
		}
		
		return response;
	}

	public void phstep1(Brower b, CoreRequest r,CoreRepsonse response) {

		WebDriver d = b.getDriver();
		b.loadUrl("https://online.immi.gov.au/elp/app?action=new&formId=VSS-AP-600");
		b.waitSecondToClick(By.xpath("//input[@type='checkbox']"), 20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-1"));
		response.setStep("1");
		d.findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep2(Brower b, CoreRequest r,CoreRepsonse response) {
		WebDriver d = b.getDriver();
		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3750']/span/span/input[@type='radio' and @value='1']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-2-1"));
		new Select(b.waitSecond(By.xpath("//select[@title='Current location']"), 5)).selectByValue("PRCH");
		new Select(d.findElement(By.xpath("//select[@title='Legal status']"))).selectByValue("1");
		d.findElement(By.xpath("//input[@type='radio' and @value='29']")).click();
		new Select(d.findElement(By.xpath("//select[contains(@title, 'mfc_option')]")))
				.selectByValue(r.getVisInfo().getVistitReason());
		WebElement significantDate = d.findElement(By.xpath(
				"//textarea[@title='Give details of any significant dates on which the applicant needs to be in Australia']"));
		significantDate.sendKeys("from " + r.getOrd().getStrokeStartDate() + " to " + r.getOrd().getStrokeStartDate());
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F4001']/span/span/input[@type='radio' and @value='2']"))
				.click();

		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3704']/span/span/input[@type='radio' and @value='2']"),
				20).click();

		response.addImage(b.printScreen(r.getTaskId(), "r-2-2"));
		response.setStep("2");
		d.findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep3(Brower b, CoreRequest r,CoreRepsonse response) {
		WebDriver d = b.getDriver();
		b.waitSecond(By.xpath("//input[@type='text' and @title='Family name']"), 20)
				.sendKeys(r.getBinfo().getFamilyName());
		response.addImage(b.printScreen(r.getTaskId(), "r-3-1"));
		d.findElement(By.xpath("//input[@type='text' and @title='Given names']")).sendKeys(r.getBinfo().getGivenName());

		d.findElement(By.xpath(getSexXpath(r.getBinfo().getSex()))).click();
		d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of birth Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(r.getBinfo().getBirth()));
		d.findElement(By.xpath("//input[@type='text' and @title='Passport number']"))
				.sendKeys(r.getBinfo().getPassport());
		new Select(d.findElement(By.xpath("//select[@title='Country of passport']"))).selectByValue("CHN");
		new Select(b.waitSecond(By.xpath("//select[@title='Place of issue - China']"), 5))
				.selectByValue(r.getBinfo().getPlaceIssure());
		new Select(d.findElement(By.xpath("//select[@title='Nationality of passport holder']"))).selectByValue("CHN");
		d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of issue Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(r.getBinfo().getDateIssue()));
		d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of expiry Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(r.getBinfo().getDateExpiry()));

		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3900']/span/span/input[@type='radio' and @value='1']"))
				.click();
		response.addImage(b.printScreen(r.getTaskId(), "r-3-2"));

		b.waitSecondToClick(By.xpath("//button[@type='submit' and @value='x' and text()='Add']"), 10).click();

		// step3 - idcard------------start--
		b.waitSecondToClick(By.xpath("//input[@type='text' and @title='Identification number']"), 10)
				.sendKeys(r.getIdcard().getIdNum());
		response.addImage(b.printScreen(r.getTaskId(), "r-3-id-1"));
		d.findElement(By.xpath("//input[@type='text' and @title='Family name']"))
				.sendKeys(r.getIdcard().getFamilyName());
		d.findElement(By.xpath("//input[@type='text' and @title='Given names']"))
				.sendKeys(r.getIdcard().getGivenName());
		new Select(d.findElement(By.xpath("//select[@title='Country of issue']"))).selectByValue("PRCH");
		d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of issue Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(r.getIdcard().getDateIssue()));
		d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of expiry Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(r.getIdcard().getDateExpiry()));

		d.findElement(By.xpath("//button[@title='Save the current entry'][1]")).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-3-id-2"));
		// step3 - idcard------------end--

		b.waitSecondToClick(By.xpath("//input[@type='text' and @title='Town / City']"), 20)
				.sendKeys(r.getBinfo().getCity());
		d.findElement(By.xpath("//input[@type='text' and @title='State / Province']"))
				.sendKeys(r.getBinfo().getProvince());
		new Select(d.findElement(By.xpath("//select[@title='Country of birth']"))).selectByValue("PRCH");
		new Select(d.findElements(By.xpath("//select[@title='Relationship status']")).get(1))
				.selectByValue(r.getBinfo().getRelationship());
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0760']/span/span/input[@type='radio' and @value='2']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0240']/span/span/input[@type='radio' and @value='1']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0790']/span/span/input[@type='radio' and @value='2']"))
				.click();

		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0480']/span/span/input[@type='radio' and @value='2']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0850']/span/span/input[@type='radio' and @value='2']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0411']/span/span/input[@type='radio' and @value='2']"))
				.click();
		response.addImage(b.printScreen(r.getTaskId(), "r-3-3"));
		response.setStep("3");
		d.findElement(By.xpath("//button[@title='Go to next page']")).click();

	}

	private String getSexXpath(String sex) {
		String sexXpth = "//input[@type='radio' and @value='${value}']";
		Map<String, String> params = Maps.newHashMap();
		params.put("value", sex);
		String realXpath = PlaceholderUtils.resolvePlaceholders(sexXpth, params);
		return realXpath;
	}

	public void phstep4(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2020']/span/span/input[@type='radio' and @value='1']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-4"));
		b.getDriver().findElement(By.xpath("//button[@title='Go to next page']")).click();
		b.waitSecond(null, 10);
		response.setStep("4");
		checkIsError(b.getDriver().getPageSource());
	}

	Pattern p = Pattern.compile("Based on the entered passport details,([^<]+)");

	public void checkIsError(String pageSource) {
		if (pageSource.contains("An error has occurred")) {
			Matcher m = p.matcher(pageSource);
			while (m.find()) {
				throw new WcAccessException(m.group(1));
			}
			throw new WcAccessException("发生页面级错误,请检查填写是否正确");
		}
	}
	Pattern txnPattern = Pattern.compile("Transaction Reference Number \\(TRN\\):\\s?([A-Za-z0-9]+)");
	public void phstep5(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3280']/span/span/input[@type='radio' and @value='2']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-5"));
		response.setStep("5");
		Matcher m = txnPattern.matcher(b.getDriver().getPageSource());
		while (m.find()) {
			response.setTrn(m.group(1));
		}
		b.getDriver().findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep6(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F4575']/span/span/input[@type='radio' and @value='2']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-6"));
		response.setStep("6");
		b.getDriver().findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep7(Brower b, CoreRequest r,CoreRepsonse response) {
		WebDriver d = b.getDriver();

		new Select(b.waitSecondToClick(By.xpath("//select[@title='Usual country of residence']"), 20))
				.selectByValue("PRCH");
		response.addImage(b.printScreen(r.getTaskId(), "r-7-1"));
		new Select(d.findElement(By.xpath("//select[@title='Office']"))).selectByValue("122");
		new Select(d.findElement(By.xpath("//select[@title='Country']"))).selectByValue("PRCH");

		d.findElement(By.xpath("//input[@type='text' and @title='Address']")).sendKeys(r.getBinfo().getCurrAddress1());
		d.findElement(By.xpath("//input[@type='text' and @title='Address 2']"))
				.sendKeys(r.getBinfo().getCurrAddress2());
		response.addImage(b.printScreen(r.getTaskId(), "r-7-1-1"));
		b.waitSecondToClick(
				By.xpath(
						"(//span[@class='wc-textfield wc_input_wrapper ELP-F0028'])[2]/input[@type='text' and @title='Suburb / Town']"),
				2).sendKeys(r.getBinfo().getCurrTown());
		// d.findElement(By.xpath("//input[@type='text' and @title='Suburb /
		// Town']")).sendKeys(bvinfo.getCurrTown());
		new Select(b.waitSecondToClick(By.xpath("//select[@title='State or Province']"), 10))
				.selectByValue(r.getBinfo().getCurrProvince());
		d.findElement(By.xpath("//input[@type='text' and @title='Postal code']"))
				.sendKeys(r.getBinfo().getPostalCode());

		d.findElement(By.xpath("//input[@type='text' and @title='Business phone']"))
				.sendKeys(r.getBinfo().getBusinesPhone());
		d.findElement(By.xpath("//input[@type='text' and @title='Mobile / Cell phone']"))
				.sendKeys(r.getBinfo().getMobile());
		b.waitSecond(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2110']/span/span/input[@type='radio' and @value='1']"),
				5).click();
		WebElement email = d.findElement(By.xpath("//input[@type='email' and @title='Email address']"));
		email.clear();
		email.sendKeys(r.getBinfo().getEmail());
		response.addImage(b.printScreen(r.getTaskId(), "r-7-2"));
		response.setStep("7");
		d.findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep8(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecond(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-stacked wc_noborder wc_chkgrp ELP-F0070']/div/div/input[@type='radio' and @value='NO']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-8-1"));
		WebElement email = b.waitSecondToClick(By.xpath("//input[@type='email' and @title='Email address']"), 12);
		email.clear();
		email.sendKeys(r.getBinfo().getEmail());
		response.addImage(b.printScreen(r.getTaskId(), "r-8-3"));
		response.setStep("8");
		b.getDriver().findElement(By.xpath("//button[@title='Go to next page']")).click();
	}

	public void phstep9(Brower b, CoreRequest r,CoreRepsonse response) {
		WebDriver d = b.getDriver();
		b.waitSecondToClick(
				By.xpath(
						"//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2143']/span/span/input[@type='radio' and @value='1']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-9-1"));

		if (r.getFamilyMembers() == null || r.getFamilyMembers().isEmpty()) {
			d.findElement(By
					.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2143']/span/span/input[@type='radio' and @value='2']"))
					.click();
		} else {
			for (int i = 0; i < r.getFamilyMembers().size(); i++) {
				FamilyMember fm = r.getFamilyMembers().get(i);
				b.waitSecondToClick(By.xpath("//button[@type='submit' and @value='x' and text()='Add']"), 10).click();
				b.waitSecondToClick(By.xpath("//input[@type='text' and @title='Family name']"), 10)
						.sendKeys(fm.getFamilyName());
				new Select(d.findElement(By.xpath("//select[@title='Relationship to the applicant']")))
						.selectByValue(fm.getRelationShip());
				d.findElement(By.xpath("//input[@type='text' and @title='Given names']")).sendKeys(fm.getGivenName());
				d.findElement(By.xpath(getphstep9SexXpath(fm.getSex()))).click();
				d.findElement(By.xpath("//input[@type='text' and contains(@title, 'Date of birth Shortcuts')]"))
						.sendKeys(DateUtils.toDateStringEnglish(fm.getBirthdate()));
				new Select(d.findElement(By.xpath("//select[@title='Country of birth']"))).selectByValue("PRCH");
				response.addImage(b.printScreen(r.getTaskId(), "r-9-2-" + i));
				d.findElement(By.xpath("//button[@type='submit' and @value='x' and text()='Confirm']")).click();
			}
		}

		WebElement button = b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10);
		response.addImage(b.printScreen(r.getTaskId(), "r-9-3"));
		response.setStep("9");
		button.click();

	}

	private String getphstep9SexXpath(String sex) {
		String sexXpth = "//fieldset[@class='wc-radiobuttonselect wc-layout-stacked wc_noborder wc_chkgrp ELP-F0570']/div/div/input[@type='radio' and @value='${value}']";
		Map<String, String> params = Maps.newHashMap();
		params.put("value", sex);
		String realXpath = PlaceholderUtils.resolvePlaceholders(sexXpth, params);
		return realXpath;
	}

	public void phstep10(Brower b, CoreRequest r,CoreRepsonse response) {
		WebDriver d = b.getDriver();
		new Select(b.waitSecondToClick(By.xpath("//select[@title='Length of stay in Australia']"), 20))
				.selectByValue("3");
		response.addImage(b.printScreen(r.getTaskId(), "r-10-1"));
		b.waitSecondToClick(By.xpath("//input[@type='text' and contains(@title, 'Date from Shortcuts')]"), 20)
				.sendKeys(DateUtils.toDateStringEnglish(r.getOrd().getStrokeStartDate()));
		b.waitSecondToClick(By.xpath("//input[@type='text' and contains(@title, 'Date to Shortcuts')]"), 20)
				.sendKeys(DateUtils.toDateStringEnglish(r.getOrd().getStrokeEndDate()));
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3707']/span/span/input[@type='radio' and @value='2']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3780']/span/span/input[@type='radio' and @value='2']"))
				.click();
		d.findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F3785']/span/span/input[@type='radio' and @value='2']"))
				.click();
		response.addImage(b.printScreen(r.getTaskId(), "r-10-2"));
		response.setStep("10");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}

	public void phstep12(Brower b, CoreRequest r,CoreRepsonse response) {
		EmployInfo e = r.getEmployInfo();
		WebDriver d = b.getDriver();
		new Select(b.waitSecondToClick(By.xpath("//select[@title='Employment status']"), 20)).selectByValue("1");
		response.addImage(b.printScreen(r.getTaskId(), "r-12-1"));

		new Select(b.waitSecondToClick(By.xpath("//select[@title='Occupation grouping']"), 5))
				.selectByValue(e.getOccupationGroup());
		if (e.getOccupationGroup().equals("070299")) {
			b.waitSecondToClick(By.xpath("//input[@type='text' and @title='Occupation']"), 5)
					.sendKeys(e.getOccupation());
		}
		d.findElement(By.xpath("//input[@type='text' and @title='Organisation']")).sendKeys(e.getOrganisation());
		d.findElement(
				By.xpath("//input[@type='text' and contains(@title, 'Start date with current employer Shortcuts')]"))
				.sendKeys(DateUtils.toDateStringEnglish(e.getDateEmployer()));

		new Select(d.findElement(By.xpath("//select[@title='Country']"))).selectByValue("PRCH");
		d.findElement(By.xpath("//input[@type='text' and @title='Address']")).sendKeys(e.getAddress1());
		d.findElement(By.xpath("//input[@type='text' and @title='Address 2']")).sendKeys(e.getAddress2());
		d.findElement(By.xpath("(//input[@type='text' and @title='Suburb / Town'])[2]")).sendKeys(e.getTown());

		new Select(b.waitSecondToClick(By.xpath("//select[@title='State or Province']"), 5))
				.selectByValue(e.getProvince());
		d.findElement(By.xpath("//input[@type='text' and @title='Postal code']")).sendKeys(e.getPostalCode());
		d.findElement(By.xpath("//input[@type='text' and @title='Business phone']")).sendKeys(e.getPhone());
		d.findElement(By.xpath("//input[@type='text' and @title='Mobile / Cell phone']")).sendKeys(e.getMobile());
		d.findElement(By.xpath("//input[@type='email' and @title='Email address']")).sendKeys(e.getEmail());

		response.addImage(b.printScreen(r.getTaskId(), "r-12-2"));
		response.setStep("12");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}

	public void phstep13(Brower b, CoreRequest r,CoreRepsonse response) {
		new Select(b.waitSecondToClick(
				By.xpath("//select[@title='Give details of how the stay in Australia will be funded.']"), 20))
						.selectByValue("1");
		b.getDriver()
				.findElement(By
						.xpath("//textarea[@title='What funds will the applicant have available to support their stay in Australia?']"))
				.sendKeys(r.getVisInfo().getFundingStayInfo());
		response.addImage(b.printScreen(r.getTaskId(), "r-13-1"));
		response.setStep("13");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}

	public void phstep16(Brower b, CoreRequest r,CoreRepsonse response) {

		b.waitSecondToClick(
				By.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0390']/span/span/input[@type='radio' and @value='2']"),
				20).click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0400']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0410']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0430']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6105']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0443']/span/span/input[@type='radio' and @value='2']"))
				.click();
		response.addImage(b.printScreen(r.getTaskId(), "r-16-1"));
		response.setStep("16");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}

	public void phstep17(Brower b, CoreRequest r,CoreRepsonse response) {

		b.waitSecondToClick(
				By.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0110']/span/span/input[@type='radio' and @value='2']"),
				20).click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0120']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6151']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6152']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6153']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0100']/span/span/input[@type='radio' and @value='2']"))
				.click();
		
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6155']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0190']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6156']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6157']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6158']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6163']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6160']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0140']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0180']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F6162']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0160']/span/span/input[@type='radio' and @value='2']"))
				.click();
		response.addImage(b.printScreen(r.getTaskId(), "r-17-1"));
		response.setStep("17");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}
	
	public void phstep18(Brower b, CoreRequest r,CoreRepsonse response) {
		VisaInfo h = r.getVisInfo();
		b.waitSecondToClick(
				By.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2159']/span/span/input[@type='radio' and @value='"+h.getHastoOtherCountry()+"']"),
				20).click();
		if(h.getHastoOtherCountry().equals("1")){
			b.getDriver().findElement(By.xpath("(//textarea[ @title='Give details'])[1]")).sendKeys(h.getHastoOtherCountryDetails());
		}
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2152']/span/span/input[@type='radio' and @value='2']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2156']/span/span/input[@type='radio' and @value='"+h.getHasRefuse()+"']"))
				.click();
		if(h.getHasRefuse().equals("1")){
			b.getDriver().findElement(By.xpath("(//textarea[ @title='Give details'])[3]")).sendKeys(h.getHasRefuseDetails());
		}
		response.addImage(b.printScreen(r.getTaskId(), "r-18-1"));
		response.setStep("18");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}
	
	public void phstep20(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecondToClick(
				By.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0350']/span/span/input[@type='radio' and @value='1']"),
				20).click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0320']/span/span/input[@type='radio' and @value='1']"))
				.click();
		
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0351']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0356']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0280']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0348']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0349']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0270']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F4326']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F4327']/span/span/input[@type='radio' and @value='1']"))
				.click();
	
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0251']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F4328']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0354']/span/span/input[@type='radio' and @value='1']"))
				.click();
		b.getDriver().findElement(By
				.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F0355']/span/span/input[@type='radio' and @value='1']"))
				.click();
		
		response.addImage(b.printScreen(r.getTaskId(), "r-20-1"));
		response.setStep("20");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}
	
	public void phstep21(Brower b, CoreRequest r,CoreRepsonse response) {
		b.waitSecondToClick(
				By.xpath("//fieldset[@class='wc-radiobuttonselect wc-layout-flat wc_noborder wc_chkgrp wc-hgap-med ELP-F2200']/span/span/input[@type='radio' and @value='2']"),
				20).click();
		response.addImage(b.printScreen(r.getTaskId(), "r-21-1"));
		response.setStep("21");
		b.waitSecondToClick(By.xpath("//button[@title='Go to next page']"), 10).click();
	}
	
	
	public void confirmPage(Brower b, CoreRequest r,CoreRepsonse response){
		WebElement button =b.waitSecondToClick(By.xpath("//button[@title='Go to the next page']"), 20);
		response.addImage(b.printScreen(r.getTaskId(), "r-confirm-1"));
		response.setStep("confirm");
		button.click();
		
	}
}
