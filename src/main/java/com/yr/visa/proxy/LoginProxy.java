package com.yr.visa.proxy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yr.visa.connection.ConfigManager;
import com.yr.visa.proxy.ctx.Brower;
import com.yr.visa.proxy.ctx.WcAccessException;

@Component
public class LoginProxy {

	@Autowired
	ConfigManager configManager;
	
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public Brower login()  {
		Brower  b = new Brower(configManager);
		b.getDriver().get("https://online.immi.gov.au/lusc/login");
		
        WebElement ele = b.waitSecondToClick(By.id("_2b0a0a3a1a_input"), 20);
        b.printScreen(null, "login-1-1");
        ele.sendKeys(configManager.getSysConfig("username"));
        b.getDriver().findElement(By.id("_2b0a0a3b1a")).sendKeys(configManager.getSysConfig("password"));
        b.printScreen(null, "login-1-2");
        b.getDriver().findElement(By.id("_2b0a0a4a1a0")).click();
        if(!b.getDriver().getTitle().contains("Login successful")){
        	b.getDriver().quit();
        	throw new WcAccessException("登录失败");
        }
       
        WebElement continueEle= b.waitSecondToClick(By.id("_2b0a0b0e0b0a"),10);
        b.printScreen(null, "login-2-");
        continueEle.click();
        		
        return b;
       
	}

	public ConfigManager getConfigManager() {
		return configManager;
	}

	public void setConfigManager(ConfigManager configManager) {
		this.configManager = configManager;
	}
	
	
	
}
