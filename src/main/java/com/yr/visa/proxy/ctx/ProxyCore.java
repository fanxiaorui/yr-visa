package com.yr.visa.proxy.ctx;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.yr.visa.util.IdFactory;

@Component
public class ProxyCore {
	private Logger logger = LoggerFactory.getLogger("processLog");
	WebClient webClient;

	Brower b;
	
	
	
	public Brower getB() {
		return b;
	}

	public WebDriver getDriver() {
		return b.getDriver();
	}

	public void setB(Brower b) {
		this.b = b;
	}

	public WebClient getWebClient() {
		return webClient;
	}

	public HtmlPage getPage(String url) {
		try {
			HtmlPage page = webClient.getPage(url);
			webClient.waitForBackgroundJavaScript(3 * 1000l);
			logger.info(url + "-" + page.asText());
			return page;
		} catch (FailingHttpStatusCodeException | IOException e) {
			logger.error("", e);
			throw new WcAccessException("", e);
		}
	}

	@PostConstruct
	public void init() {
		this.webClient = WebClientUtil.getNewInstanceWebClient();
	}

	public void printNodeListSelenium(){
		logger.info("----start--------------------------------------------------");
		List<WebElement> ddes =getDriver().findElements(By.tagName("input"));
		if(ddes != null){
			for(WebElement we: ddes){
				if (we.isEnabled()&& we.isDisplayed() && we.getAttribute("type").equals("hidden")) {
					continue;
				}
				printNodeRealValue(we);
			}
		}
		logger.info("----end:input--------");
		ddes =getDriver().findElements(By.tagName("select")); 
		if (ddes != null) {
			for(WebElement we: ddes){
				printNodeRealValue(we);
			}
		}

		logger.info("----end:select--------");
		ddes =getDriver().findElements(By.tagName("button"));
		if (ddes != null) {
			for(WebElement we: ddes){
				printNodeRealValue(we);
			}
		}
		logger.info("----end:button--------");
		logger.info("-----end-------------------------------------------------");
	}
	public void printSelectOptionsSelenium(Select select){
		List<WebElement> options =	select.getAllSelectedOptions();
		if(options != null){
			for(WebElement o : options){
				printNodeRealValue(o);
			}
		}
		logger.info("----end:option--------");
	}
	
	public void printScreen(){
		 File scrFile = ((TakesScreenshot) this.getDriver()).getScreenshotAs(OutputType.FILE);  
         try {
			FileUtils.copyFile(scrFile, new File("H:\\"+IdFactory.getDateTimeRandomStr()+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	private void printNodeRealValue(WebElement e){
	
		logger.info(e.getTagName());
	}
	
	public void printNodeList(HtmlPage p) {
		logger.info("----start--------------------------------------------------");
		DomNodeList<DomElement> ddes = p.getElementsByTagName("input");
		for (int i = 0; i < ddes.size(); i++) {
			HtmlInput domElement = (HtmlInput) ddes.get(i);
			if (domElement.getAttribute("type").equals("hidden")) {
				continue;
			}
			logger.info(domElement.toString());
		}
		logger.info("----end:input--------");
		ddes = p.getElementsByTagName("select");
		if (ddes != null) {
			for (int i = 0; i < ddes.size(); i++) {
				DomElement domElement = ddes.get(i);
				logger.info(domElement.toString());
			}
		}

		logger.info("----end:select--------");
		ddes = p.getElementsByTagName("button");
		if (ddes != null) {
			for (int i = 0; i < ddes.size(); i++) {
				DomElement domElement = ddes.get(i);
				logger.info(domElement.toString());
			}
		}
		logger.info("----end:button--------");
		logger.info("-----end-------------------------------------------------");
	}
	
	public void printSelectOptions(HtmlSelect select){
		List<HtmlOption> options =	select.getOptions();
		if(options != null){
			for(HtmlOption o : options){
				logger.info(o.toString());
			}
		}
		logger.info("----end:option--------");
	}
	public void waitJS(int second){
		this.webClient.waitForBackgroundJavaScript(second* 1000l);
	}
	public void setWebClient(WebClient webClient) {
		this.webClient = webClient;
	}

}
