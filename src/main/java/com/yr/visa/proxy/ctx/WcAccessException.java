package com.yr.visa.proxy.ctx;

public class WcAccessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2296637607091050282L;

	public WcAccessException(String str) {
		super(str);
	}

	public WcAccessException(String message, Throwable cause) {
		super(message, cause);
	}
}
