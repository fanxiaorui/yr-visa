package com.yr.visa.proxy.ctx;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.google.common.collect.Sets;

public class WebClientUtil {

	private Logger log = LoggerFactory.getLogger(WebClientUtil.class);
	
	public static WebClient getNewInstanceWebClient() {
		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		webClient.getOptions().setActiveXNative(true);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		
		webClient.getOptions().setTimeout(50000);
		webClient.getOptions().setRedirectEnabled(true);
		return webClient;
	}
	public static Set<org.apache.http.cookie.Cookie> CookiesAdapter(WebClient wd) {
		try {
			Set<com.gargoylesoftware.htmlunit.util.Cookie> cookies = wd
					.getCookieManager().getCookies();
			if (cookies != null && !cookies.isEmpty()) {
				Set<org.apache.http.cookie.Cookie> apacheCookies = Sets
						.newHashSet();
				for (com.gargoylesoftware.htmlunit.util.Cookie _c : cookies) {
					BasicClientCookie _t = new BasicClientCookie(_c.getName(),
							_c.getValue());
					_t.setDomain(_c.getDomain());
					apacheCookies.add(_t);
				}
				return apacheCookies;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	public static Page sendPostRequest(WebClient webClient,String url, Map<String, String> params)
			throws Exception {
		return sendPostRequest(webClient,url, params,HttpMethod.POST);
	}
	public static Page sendPostRequest(WebClient webClient,String url, Map<String, String> params,HttpMethod method)
			throws Exception {
		 
		WebRequest webRequest = new WebRequest(new URL(url));
		webRequest.setHttpMethod(method);
		webRequest.setRequestParameters(new ArrayList<NameValuePair>());
		if (params != null && params.size() > 0) {
			for (Entry<String, String> param : params.entrySet()) {
				webRequest.getRequestParameters().add(
						new NameValuePair(param.getKey(), param.getValue()));
			}
		}
		return sendRequest(webClient, webRequest);
	}

	// 底层请求
	private static Page   sendRequest(WebClient webClient, WebRequest webRequest)
			throws Exception {
		Page page = webClient.getPage(webRequest);
		return page;
	}
	
	
	public static  <T extends DomElement> T element(HtmlPage page,String xpath,Class<T> clazz){
		List<DomElement> _es =(List<DomElement>)(List)page.getByXPath(xpath);
		if(_es == null || _es.isEmpty()){
			return null;
		}
		
		DomElement _e =_es.get(0);
		return (T)_e;
	}
}
