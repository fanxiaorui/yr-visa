package com.yr.visa.util;

import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;



public class String2Date {
	public static String LEBAOBA_DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX";

	public static String LEBAOBA_DATEFORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss";
	public static String LEBAOBA_DATEFORMAT_3 = "yyyy-MM-dd";
	public static String LEBAOBA_DATEFORMAT_4 = "yyyy-MM-dd HH:mm:ss";
	public static String SIMPLEYEARMONTH = "yyyyMM";
	public static String SIMPLEYEAR = "yyyy";

	static Pattern p1 = Pattern
			.compile("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}\\s+[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}");
	static Pattern p2 = Pattern.compile("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}");
	static Pattern p3 = Pattern
			.compile("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\\.[0-9]+\\+.+");
	static Pattern p4 = Pattern
			.compile("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}");

	public static Date conver2Date(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		str = str.trim();

		if (str.startsWith("0001")) {
			return null;
		}
		String format = null;
		if (p2.matcher(str).matches()) {
			format = LEBAOBA_DATEFORMAT_3;
		} else if (p1.matcher(str).matches()) {
			format = LEBAOBA_DATEFORMAT_4;
		} else if (p4.matcher(str).matches()) {
			format = LEBAOBA_DATEFORMAT_2;
		} else if (p3.matcher(str).matches()) {
			format = LEBAOBA_DATEFORMAT;
		} else {

		}

		return DateUtils.parseDateByFormat(str, format);
	}

	static Pattern p5 = Pattern.compile("[0-9]{4}[0-9]{1,2}");
	static Pattern p6 = Pattern.compile("[0-9]{4}");

	public static Date simpleDate(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		str = str.trim();
		String format = null;
		if (p5.matcher(str).matches()) {
			format = SIMPLEYEARMONTH;
		} else if (p6.matcher(str).matches()) {
			format = SIMPLEYEAR;
		}

		try {
			return DateUtils.parseDateByFormat(str, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println("5(/)".substring(0, 1));
	}
}
