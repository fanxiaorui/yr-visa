package com.yr.visa.util;

import com.alibaba.fastjson.JSONObject;

/**
 * 字符串工具类。
 * 
 */
public class StringExtendsUtils {

	public static boolean isEmpty(String value) {
		int strLen;
		if (value == null || (strLen = value.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(value.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	public static String getTrim(String date) {
		if (date == null) {
			return null;
		} else {
			return date.trim();
		}
	}

	public static String getNoNull(JSONObject jo,String property){
		if(jo == null || property == null){
			return "";
		}
		String result =jo.getString(property);
		return result==null?"":result;
		
	}
	

}
