package com.yr.visa.util;

import java.math.BigDecimal;


public class StringParseUtil extends StringUtil {
	
	
	public static Double toDouble(String str){
		if(StringUtil.isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim();
			return Double.parseDouble(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toStringByBigdecimal(BigDecimal b,int scale){
		return b.setScale(scale,BigDecimal.ROUND_FLOOR).toString();
	}

}
