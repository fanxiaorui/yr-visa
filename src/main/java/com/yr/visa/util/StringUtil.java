/*
 * Copyright (C), 2002-2012, 苏宁易购电子商务有限公司
 * FileName: StringUtil.java
 * Author:   李强
 * Date:     2013-9-8
 * Description:  
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */

package com.yr.visa.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * String工具类
 * 
 * @author 无
 */
public class StringUtil {
    public static final int LENGTH_ONE = 1;
    public static final int LENGTH_TWO = 2;
    public static final int LENGTH_THREE = 3;
    public static final String STR_DOT = ".";
    public static final String MONEY_FEN = "00";
    public static final int CARD_LENGTH_14 = 14;
    public static final int CARD_LENGTH_16 = 14;

	private static boolean isMatch(String regex, String orginal){ 
    	if (orginal == null || orginal.trim().equals("")) { 
    		return false; 
    	}
    	Pattern pattern = Pattern.compile(regex); 
    	Matcher isNum = pattern.matcher(orginal); 
    	return isNum.matches(); 
	}
	
	/**
	 * 判断是否为汽车排量格式
	 * 整数为1
	 * 小数为1-3位
	 * @param orginal
	 * @return
	 */
	public static boolean isDisplacementNumber(String orginal) { 
    	return isMatch("^\\d{1}\\.\\d*", orginal);
	}
    	
    public static String matchNumberAndLetters(String str){
		if(StringUtils.isBlank(str)){
			return null;
		}
		return str.replaceAll("[^\\w^ ]|_","");
	}
    
    public static boolean isNotNull(String s) {
        if (null != s && s.trim().length() != 0 && !s.equalsIgnoreCase("null"))  {
            return true;
        }
        return false;
    }

    public static boolean isNull(String s) {
        if (null == s || s.trim().length() == 0 || s.equalsIgnoreCase("null")) {
            return true;
        }
        return false;
    }

    /**
     * 根据要求字符串长度，数字左补零
     * 
     * @author YuanBing
     * @param data
     * @param length
     * @return
     */
    public static String leftFillZero(String str, int length) {
        if (null == str || "".equals(str.trim()) || length <= 0) {
            return "";
        }
        str = str.trim();
        if (str.length() == length) {
            return str;
        }
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < length) {
            sb.append('0');
            i++;
        }
        sb.append(str);
        return sb.substring(sb.length() - length);
    }

    /**
     * 根据要求字符串长度，字符串右补空格
     * 
     * @author YuanBing
     * @param s
     * @param length
     * @return
     */
    public static String rightFillSpace(String s, int length) {
        int fillLength = 0;
        if (s != null) {
            if (s.trim().length() > length) {
                return s.trim().substring(0, length);
            } else {
                fillLength = length - s.trim().length();
            }
        } else {
            fillLength = length;
        }
        StringBuilder sb = new StringBuilder();
        if (s != null) {
            sb.append(s.trim());
        }
        for (int i = 0; i < fillLength; i++) {
            sb.append(" ");
        }
        if (sb.length() > length) {
            return sb.substring(0, length);
        }

        return sb.toString();
    }

    /**
     * 格式化钱（如：2000L--20.00）
     * 
     * @param money
     * @return
     */
    public static String formatMoney(Long money) {
        if (money == null || "".equals(String.valueOf(money).trim())) {
            return "0.00";
        }
        String m = money.toString();
        int len = m.trim().length();
        String result = "";
        switch (len) {
            case 1:
                result = "0.0" + m;
                break;
            case 2:
                result = "0." + m;
                break;
            default:
                result = m.substring(0, m.length() - LENGTH_TWO) + STR_DOT + m.substring(m.length() - LENGTH_TWO);
                break;
        }
        return result;
    }

    /**
     * 格式化钱的样式如输入：777777777777->"7,777,777,777.77"
     * 
     * @param money long型的钱
     * @return 格式化后的string 钱
     */
    public static String formatBankSpaceMoney(Long money) {
        if (money == null) {
            return null;
        }
        int mflag = 0;
        if (money < 0) {
            money = money * (-LENGTH_ONE);
            mflag = -1;
        }
        String m = money.toString();
        int len = m.trim().length();
        String result = "";
        if (len == 1) {
            result = "0.0" + m;
        } else if (len == 2) {
            result = "0." + m;
        } else {
            String s = m.substring(0, m.length() - 2);
            if (s.length() < LENGTH_THREE) {
                result = s + "." + m.substring(m.length() - 2);
            } else {
                result = "." + m.substring(m.length() - 2);
                int i = 0;
                String st = s;
                for (; st.length() >= LENGTH_THREE;) {
                    if (i == 0) {
                        result = st.substring(st.length() - LENGTH_THREE) + result;
                        i++;
                    } else {
                        result = st.substring(st.length() - LENGTH_THREE) + "," + result;
                    }
                    st = st.substring(0, st.length() - LENGTH_THREE);
                }
                if (st.length() > 0) {
                    result = st + "," + result;
                }
            }
        }
        if (mflag < 0) {
            result = "-" + result;
        }
        return result;
    }

    /**
     * 格式化钱（如："2000"--"20.00"）
     * 
     * @param money
     * @return
     */
    public static String formatMoney(String money) {
        if (money == null || "".equals(money.trim())) {
            return "0.00";
        }
        Double dTemp = Double.parseDouble(money);
        Long mon = dTemp.longValue();
        String m = mon.toString();
        int len = m.length();
        String result = "";
        switch (len) {
            case 1:
                result = "0.0" + m;
                break;
            case 2:
                result = "0." + m;
                break;
            default:
                result = m.substring(0, m.length() - 2) + "." + m.substring(m.length() - 2);
                break;
        }
        return result;
    }

    /**
     * 将string 格式的钱转换为long（如："20.00"--2000L） 输入格式必须为"20.00"
     * 
     * @param money
     * @return
     */
    public static Long moneyToLong(String money) {
        if (money == null) {
            return null;
        }
        money = money.trim();
        if (money.indexOf(".") > 0) {
            int index = money.indexOf(".");
            int len = money.length();
            if (len - index >= LENGTH_THREE) {
                money = money.substring(0, index) + money.substring(index + LENGTH_ONE, index + LENGTH_THREE);
            } else if (len - index == LENGTH_TWO) {
                money = money.substring(0, index) + money.substring(index + 1) + "0";
            } else {
                money = money.substring(0, index) + MONEY_FEN;
            }
        } else {
            money = money + MONEY_FEN;
        }
        return Long.valueOf(money);
    }

    /**
     * 将string 格式的钱转换为long（如："20.0或20.00"--2000L） 输入格式必须为
     * 
     * @param money
     * @return
     */
    public static Long money2Long(String money) {
        if (money == null) {
            return null;
        }
        money = money.trim();
        if (money.indexOf(".") > 0) {
            int index = money.indexOf(".");
            if (money.length() - index >= LENGTH_THREE) {
                money = money.substring(0, index) + money.substring(index + LENGTH_ONE, index + LENGTH_THREE);
            } else if (money.length() - index == 2) {
                money = money.substring(0, index) + money.substring(index + LENGTH_ONE) + "0";
            } else {
                money = money.substring(0, index) + MONEY_FEN;
            }
        } else {
            money = money + MONEY_FEN;
        }
        return Long.valueOf(money);
    }

    /**
     * 中间方法:例如20.00--2000 4r.5t--4r5t f57.t--f57t0
     * */
    public static String stringToString(String money) {
        if (money == null) {
            return null;
        }
        money = money.trim();
        if (money.indexOf(".") > 0) {
            int index = money.indexOf(".");
            int len = money.length();
            if (len - index >= 3) {
                money = money.substring(0, index) + money.substring(index + 1, index + 3);
            } else if (len - index == 2) {
                money = money.substring(0, index) + money.substring(index + 1) + "0";
            } else {
                money = money.substring(0, index) + MONEY_FEN;
            }
        } else {
            money = money + MONEY_FEN;
        }
        return money;
    }

    /**
     * 将string 格式的钱转换为long（如："20.00"--2000） 输入格式必须为"20.00"
     * 
     * @param money
     * @return
     */
    public static String moneyToString(String money) {
        if (money == null) {
            return null;
        }
        return moneyToLong(money).toString();
    }

    /**
     * 将string 格式的钱转换为string（如："20.0或20.00"--2000） 输入格式必须为
     * 
     * @param money
     * @return
     */
    public static String money2String(String money) {
        if (money == null) {
            return null;
        }
        return money2Long(money).toString();
    }

    /**
     * 去除字符串中的空格,换行,制表符
     * 
     * @param s
     * @return
     */
    public static String trim(String s) {
        Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = p.matcher(s);
        String after = m.replaceAll("");
        return after;
    }

    public static String[] split(String s, String regex) {
        String[] sa = s.split(regex);

        return sa;
    }

    /**
     * 把库号+内卡号转为14位的卡号给pos用
     */
    public static String cutString(String cardId) {
        if (null == cardId || "".equals(cardId.trim())) {
            return null;
        }
        if (cardId.length() <= CARD_LENGTH_14) {
            return cardId;
        } else {
            return cardId.substring(cardId.length() - CARD_LENGTH_14);
        }
    }

    /**
     * 卡号不足16位，在前面补0
     * 
     * @param cardNum
     * @return
     */
    public static String make16DigitCardNum(String cardNum) {
        String systemCardNum = cardNum;
        int cardLength = cardNum.length();
        if (cardLength < CARD_LENGTH_16) {
            for (int i = 0; i < CARD_LENGTH_16 - cardLength; i++) {
                systemCardNum = "0" + systemCardNum;
            }
        }

        return systemCardNum;
    }

    /**
     * 
     * 判断是否相等
     * 
     * @param obj1
     * @param obj2
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static boolean equals(Object obj1, Object obj2) {
        return obj1 == null ? obj2 == null : obj1.equals(obj2);
    }

    public static boolean equalsStrict(String str1, String str2) {
        if (null == str1 || null == str2) {
            return false;
        }
        String trimStr1 = StringUtil.trim(str1);
        String trimStr2 = StringUtil.trim(str2);
        if (StringUtils.isEmpty(trimStr1) || StringUtils.isEmpty(trimStr2)) {
            return false;
        }
        return trimStr1.equals(trimStr2);
    }

    /**
     * 判断输入的字符串是否匹配
     * 
     * @param inputStr 输入字符串
     * @param match 匹配模式
     * @return
     */
    public static boolean isStrMatches(String inputStr, String match) {
        boolean isMatch = false;
        if (isNull(inputStr) || isNull(match)) {
            return isMatch;
        } else {
            Pattern pattern = Pattern.compile(match);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isMatch = true;
            }
        }
        return isMatch;
    }
    
    /**
     * 截取字符串，长度大于maxLength的字符串，保留前maxLength位加"..."
     * 
     * @param s
     * @return
     */
    public static String cutString(String s, int maxLength) {
        if (maxLength > 0) {
            if (s != null && s.length() > maxLength) {
                return s.substring(0, maxLength) + "...";
            }
        }
        return s;
    }
    
    /**
     * 千分位加逗号
     */
    public static String toStrMicrometer(String str){
        BigDecimal b = new BigDecimal(str); 
        DecimalFormat d1 =new DecimalFormat("#,##0.######");
        str = d1.format(b);
        return str;
    }
    public static boolean isBlank(String str)
    {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotBlank(String str)
    {
        return !isBlank(str);
    }
    
    static Pattern p = Pattern.compile("^([0-9]+)\\.0+$");
    public static String valueOf(Object str){
    	
    	
    	if(str == null && (str instanceof BigDecimal || str instanceof Integer)){
    		return "0";
    	}else if(str==null){
    		return "";
    	}
    	
    	String result =String.valueOf(str);
    	if(str instanceof BigDecimal ){
    		Matcher matcher= 	p.matcher(result);
    		if(matcher.matches()){
    			result = matcher.group(1);
    		}
    	}
    	
    	
    	return result;
    }
    public static String valueOf(Object str,String defaultValue){
    	defaultValue=	defaultValue == null?"":defaultValue;
    	
    	if(str == null && (str instanceof BigDecimal || str instanceof Integer)){
    		return defaultValue;
    	}
    	if(str==null){
    		return defaultValue;
    	}
    	
    	
    	
    	return String.valueOf(str);
    }
    
    public static BigDecimal toBigdecimal(String str){
    	if(isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim().replace(",", "");
			return new BigDecimal(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    public static Integer toInteger(String str){
    	if(isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim();
			return Integer.parseInt(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    public static Float toFloat(String str){
    	if(isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim();
			return Float.parseFloat(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    public static Short toShort(String str){
    	if(isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim();
			return Short.parseShort(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    public static Double toDouble(String str){
    	if(isBlank(str)){
    		return null;
    	}
    	
    	try {
    		str=str.trim();
			return Double.parseDouble(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    public static String indexByregex(String str,String regex,int index){
    	if(isNull(str)){
    		return null;
    	}
    	String[] splitsArray =str.split(regex);
    	if(splitsArray.length==1 && index>=1){
    		return null;
    	}
    	return splitsArray[index];
    }
    
    public static <T> T judgeValue(String str,String judgeValue,T defaultValue,T notequailsValue){
    	if(isNull(str)){
    		return null;
    	}
    	String _str = str.trim();
    	if(_str.equalsIgnoreCase(judgeValue)){
    		return defaultValue;
    	}
    	return notequailsValue;
    }
//    public static void main(String[] args) {
//		BigDecimal b = new BigDecimal("34.00");
//		System.out.println(StringUtil.valueOf(b));
//		System.out.println(StringUtil.isDisplacementNumber("0.2"));
//		System.out.println(StringUtil.isDisplacementNumber("0.90"));
//		System.out.println(StringUtil.isDisplacementNumber("0.209"));
//		System.out.println(StringUtil.isDisplacementNumber("1.000"));
//		System.out.println(StringUtil.isDisplacementNumber("3dk.01"));
//	}
}
