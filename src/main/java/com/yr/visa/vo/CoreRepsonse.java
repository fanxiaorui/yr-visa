package com.yr.visa.vo;

import java.util.List;

import com.google.common.collect.Lists;
import com.yr.visa.util.StringUtil;
import com.yr.visa.util.SuperBean;

public class CoreRepsonse extends SuperBean {
	
	private String taskId;
	
	private String trn;
	
	private String step;
	
	private String status="-1";
	
	private String errorImage;
	
	private List<String> processImages;

	public void addImage(String image){
		if(processImages == null){
			processImages = Lists.newLinkedList();
		}
		if(StringUtil.isNotBlank(image))
			processImages.add(image);
	}
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTrn() {
		return trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorImage() {
		return errorImage;
	}

	public void setErrorImage(String errorImage) {
		this.errorImage = errorImage;
	}

	public List<String> getProcessImages() {
		return processImages;
	}

	public void setProcessImages(List<String> processImages) {
		this.processImages = processImages;
	}
	
	

}
