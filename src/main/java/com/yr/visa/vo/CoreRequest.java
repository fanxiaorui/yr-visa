package com.yr.visa.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.Ord;
import com.yr.visa.core.model.sys.VisaInfo;
import com.yr.visa.util.SuperBean;

public class CoreRequest extends SuperBean{

	@JsonIgnore
	private String taskId;
	private BasicInfo binfo;

	private IdcardInfo idcard;
	


	private List<FamilyMember> familyMembers;

	private EmployInfo employInfo;
	private Ord ord;
	
	private VisaInfo visInfo;
	
	/**
	 * 0:暂存
	 * 1:完整提交/更新
	 */
	private int saveStatus;

	

	public int getSaveStatus() {
		return saveStatus;
	}

	public void setSaveStatus(int saveStatus) {
		this.saveStatus = saveStatus;
	}

	public Ord getOrd() {
		return ord;
	}

	public void setOrd(Ord ord) {
		this.ord = ord;
	}

	public VisaInfo getVisInfo() {
		return visInfo;
	}

	public void setVisInfo(VisaInfo visInfo) {
		this.visInfo = visInfo;
	}

	

	public EmployInfo getEmployInfo() {
		return employInfo;
	}

	public void setEmployInfo(EmployInfo employInfo) {
		this.employInfo = employInfo;
	}

	public List<FamilyMember> getFamilyMembers() {
		return familyMembers;
	}

	public void setFamilyMembers(List<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}

	



	

	
	

	public String getTaskId() {
		return taskId==null?ord.getId()+"":taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public BasicInfo getBinfo() {
		return binfo;
	}

	public void setBinfo(BasicInfo binfo) {
		this.binfo = binfo;
	}

	public IdcardInfo getIdcard() {
		return idcard;
	}

	public void setIdcard(IdcardInfo idcard) {
		this.idcard = idcard;
	}

}
