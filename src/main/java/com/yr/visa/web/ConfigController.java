package com.yr.visa.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yr.visa.connection.ConfigManager;
import com.yr.visa.core.service.SysConfigServices;
import com.yr.visa.util.R;

@Controller
@RequestMapping(value = "/config")
public class ConfigController {

	@Autowired
	SysConfigServices sysConfigServices;
	
	@Autowired
	ConfigManager configManager;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model view){
		
		view.addAttribute("config", sysConfigServices.getAll());
		return "/config/index";
	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public R<Boolean> update(@RequestParam Map<String, String> configs){
		
		sysConfigServices.updateAll(configs);
		configManager.initDbConfig();
		return R.ok();
	}
	
}
