package com.yr.visa.web.config;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yr.visa.util.R;




@ControllerAdvice
public class ExceptionHanler {

	private Logger log = LoggerFactory.getLogger(ExceptionHanler.class);
	
	

	
	
	@ExceptionHandler({RuntimeException.class})
	@ResponseBody
    public R handleSQLException(HttpServletRequest request, Exception ex) {  
		log.error("",ex);
		return new R(false, 500, "操作异常！");
    }
}
