package com.yr.visa.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.yr.visa.connection.ConfigManager;


@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Autowired
	ConfigManager configManager;
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("redirect:/main/index");
		registry.addViewController("/error").setViewName("error");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);

	}
	
	@Override  
    public void addInterceptors(InterceptorRegistry registry) {  
       
       // registry.addInterceptor(new SimpleAuthenInterceptor()).addPathPatterns("/**");  
        super.addInterceptors(registry);  
    }  

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/static/**").addResourceLocations(
				ResourceUtils.CLASSPATH_URL_PREFIX + "/static/");
		registry.addResourceHandler("/files/**").addResourceLocations(
				ResourceUtils.FILE_URL_PREFIX + configManager.getConfig().getImageSavePath());
		super.addResourceHandlers(registry);
	}
}
