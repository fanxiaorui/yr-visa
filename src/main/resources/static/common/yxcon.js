var yxconstrant={
		reship:{
			 	39:"Aunt" ,
				35:"Brother" ,
				1:"Child" ,
				31:"Cousin" ,
				63:"Daughter/Son-in-law" ,
				13:"Fiance/Fiancee " ,
				33:"Friend " ,
				19:"Grand Child " ,
				18:"Grand Parent " ,
				62:"Mother/Father-in-law " ,
				42:"Nephew " ,
				41:"Niece " ,
				2:"Parent " ,
				36:"Sister " ,
				64:"Sister/Brother-in-law " ,
				3:"Spouse/De Facto Partner " ,
				15:"Step Child " ,
				14:"Step Parent " ,
				37:"Step-brother " ,
				38:"Step-sister " ,
				40:"Uncle " 
		}
}

function reship(key){
	return yxconstrant.reship[key];
}


