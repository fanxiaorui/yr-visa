package com.yr.visa.proxy;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.yr.visa.util.DateUtils;
import com.yr.visa.vo.CoreRepsonse;
import com.yr.visa.vo.CoreRequest;
import com.yr.visa.core.model.sys.BasicInfo;
import com.yr.visa.core.model.sys.EmployInfo;
import com.yr.visa.core.model.sys.FamilyMember;
import com.yr.visa.core.model.sys.IdcardInfo;
import com.yr.visa.core.model.sys.Ord;
import com.yr.visa.core.model.sys.VisaInfo;


public class ApplicationProxyTest extends BasicTest {

	ApplicationProxy applicationProxy = new ApplicationProxy();
	
	
	
	
	
	@Test
	public void testStep1() {
		CoreRequest r = new CoreRequest();
				
		Ord o = new Ord();
		r.setOrd(o);
		o.setStrokeStartDate(DateUtils.parseDateByFormat("2017-10-20", "yyyy-MM-dd"));
		o.setStrokeEndDate(DateUtils.parseDateByFormat("2017-11-20", "yyyy-MM-dd"));
		o.setId(1l);
		
		BasicInfo b = new BasicInfo();
		b.setFamilyName("whiroger");
		b.setGivenName("fan");
		b.setSex("F");
		b.setBirth(DateUtils.parseDateByFormat("1982-12-20", "yyyy-MM-dd"));
		b.setPassport("E62254425");
	
		b.setDateIssue(DateUtils.parseDateByFormat("2017-12-20", "yyyy-MM-dd"));
		b.setDateExpiry(DateUtils.parseDateByFormat("2027-12-20", "yyyy-MM-dd"));
		b.setPlaceIssure("BJ");
		b.setCity("Nanjing");
		b.setProvince("jiangsu");
		//b.setRelationship("Married");
		b.setCurrAddress1("jiangsu street1");
		b.setCurrAddress2("nnajing 1");
		b.setCurrProvince("JS");
		b.setCurrTown("nnajing");
		b.setPostalCode("2100001");
		b.setMobile("15380785262");
		b.setBusinesPhone("02584265555");
		b.setEmail("5214152@qq.com");
		b.setRelationship("M");
		
		IdcardInfo i = new IdcardInfo();
		i.setFamilyName("roger");
		i.setGivenName("fan");
		i.setIdNum("320882198708150012");
	//	i.setCountryIssue("CHINA");
		i.setDateIssue(DateUtils.parseDateByFormat("2015-12-20", "yyyy-MM-dd"));
		i.setDateExpiry(DateUtils.parseDateByFormat("2025-12-20", "yyyy-MM-dd"));
		
		
		r.setBinfo(b);
		r.setIdcard(i);
		r.setFamilyMembers(this.members());
		r.setEmployInfo(this.employ());
		
		r.setVisInfo(this.vs());
		CoreRepsonse response =applicationProxy.coreRequest(super.b, r);
		System.out.println(response);
	}
	
	
	
	private com.yr.visa.core.model.sys.EmployInfo employ(){
		EmployInfo e = new EmployInfo();
		e.setAddress1("nanjing gulou street1");
		e.setAddress2("yi rong zaixian");
		e.setDateEmployer(DateUtils.parseDateByFormat("2015-12-20", "yyyy-MM-dd"));
		e.setEmail("360258945@qq.com");
		//e.setEmployStatus("070299");
		e.setMobile("15285255");
		e.setOccupationGroup("070299");
		e.setOccupation("customer designer");
		e.setOrganisation("yrzx company");
		e.setPhone("025554255");
		e.setPostalCode("2185855");
		e.setTown("NANJING");
		e.setProvince("JS");
		return e;
	}
	
	private VisaInfo vs(){
		VisaInfo vh = new VisaInfo();
		vh.setHastoOtherCountry("1");
		vh.setHastoOtherCountryDetails("tailand 2015");
		vh.setHasRefuse("2");
		vh.setVistitReason("2");
		vh.setFundingStayInfo("all the travel expenses during this trip will be paid by myself");
		return vh;
	}
	
	private List<FamilyMember> members(){
		List<FamilyMember> fms = Lists.newLinkedList();
		FamilyMember fm = new FamilyMember();
		fm.setId(1l);
		fm.setTogeth("1");
		fm.setBirthdate(DateUtils.parseDateByFormat("1986-12-20", "yyyy-MM-dd"));
		fm.setFamilyName("zhang");
		fm.setGivenName("san");
		fm.setRelationShip("13");
		fm.setSex("F");
		fms.add(fm);
		fm = new FamilyMember();
		fm.setId(2l);
		fm.setTogeth("0");
		fm.setBirthdate(DateUtils.parseDateByFormat("1987-12-20", "yyyy-MM-dd"));
		fm.setFamilyName("yan");
		fm.setGivenName("san");
		fm.setRelationShip("31");
		fm.setSex("M");
		fms.add(fm);
		
		return fms;
	}

}
