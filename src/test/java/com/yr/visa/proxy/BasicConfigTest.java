package com.yr.visa.proxy;

import org.junit.BeforeClass;

import com.yr.visa.connection.AppConfig;
import com.yr.visa.connection.ConfigManager;

public class BasicConfigTest {

	static ConfigManager man = new ConfigManager();
	@BeforeClass
	public static void initConfig(){
		
		AppConfig config = new AppConfig();
		//config.setIsDebug("true");
		config.setElementGetTimeout("4");
		config.setImageSavePath("H:\\visa");
		config.setLoadPageTimeout("10");
		//config.setPassword("Success1987");
		//config.setUsername("rogerfan");
		config.setPhantPath("D:\\software\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		man.setConfig(config);
	}
}
