package com.yr.visa.proxy;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.google.common.collect.Sets;
import com.yr.visa.proxy.ctx.Brower;
import com.yr.visa.proxy.ctx.ProxyCore;
import com.yr.visa.proxy.ctx.WebClientUtil;

public class BasicTest extends BasicConfigTest{

	//protected ProxyCore proxyCore = new ProxyCore();
	Brower b = null;
	@Before
	public void initCore() {
//		WebClient client = WebClientUtil.getNewInstanceWebClient();
//		parseCookie(client);
//		proxyCore.setWebClient(client);
		BasicConfigTest.man.cleanupTmpPath();
		b = new Brower(BasicConfigTest.man, parseCookieSelenium());
		//proxyCore.setB(b);
	}
	@After
	public void cleanup(){
		b.getDriver().quit();
	}
	public Set<org.openqa.selenium.Cookie> parseCookieSelenium(){
		Set<org.openqa.selenium.Cookie> cs = Sets.newLinkedHashSet();
		try {
			String _cookie = FileUtils.readFileToString(
					new File("G:\\visa.txt"), "gbk");
			_cookie = _cookie.trim().substring(7, _cookie.length());
			System.out.println(_cookie);
			for (String str : _cookie.split(";")) {
				String _str = str.trim();
				int name = _str.indexOf("=");
				String prefix = _str.substring(0, name);
				String rvalue = _str.substring(name + 1, _str.length());
				cs.add(new org.openqa.selenium.Cookie(prefix, rvalue,".online.immi.gov.au","/",null));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cs;
	}
	
	public void parseCookie(WebClient client) {
		try {
			String _cookie = FileUtils.readFileToString(
					new File("G:\\visa.txt"), "gbk");
			_cookie = _cookie.trim().substring(7, _cookie.length());
			System.out.println(_cookie);
			for (String str : _cookie.split(";")) {
				String _str = str.trim();
				int name = _str.indexOf("=");
				String prefix = _str.substring(0, name);
				String rvalue = _str.substring(name + 1, _str.length());
				client.getCookieManager().addCookie(
						new Cookie("online.immi.gov.au", prefix, rvalue));
				System.out.println(prefix + "=" + rvalue);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testParse() {
		String str = "PD-S-SESSION-ID=5Vv70Vb67lWRWYSGnk0fKA==:1_2_1_v+T6rrpfrzw0Kpag7NBr4+ybR4Q3z90xc38-bae5elZrB9vK|";
		int name = str.indexOf("=");
		System.out.println(str.substring(0, name));
		System.out.println(str.substring(name + 1, str.length()));
	}
}
