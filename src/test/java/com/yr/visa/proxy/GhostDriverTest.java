package com.yr.visa.proxy;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GhostDriverTest {

	@Test
	public void test1(){
		 DesiredCapabilities DesireCaps = new DesiredCapabilities();
	        DesireCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "D:\\software\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
	        WebDriver driver=new PhantomJSDriver(DesireCaps);
	        driver.get("http://baidu.com");
	        System.out.println(driver.getPageSource());
	}
	
	@Test
	public void testLogin() throws InterruptedException{
		ArrayList<String> cliArgsCap = new ArrayList<String>();
        cliArgsCap.add("--web-security=false");
        cliArgsCap.add("--ssl-protocol=any");
        cliArgsCap.add("--ignore-ssl-errors=true");
        cliArgsCap.add("--load-images=no");
        cliArgsCap.add("--disk-cache=yes");
     
		
				
			DesiredCapabilities DesireCaps = new DesiredCapabilities();
			DesireCaps.setCapability("acceptSslCerts", true);
			DesireCaps.setJavascriptEnabled(true);
			DesireCaps.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0");
			DesireCaps.setCapability("phantomjs.page.customHeaders.User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:50.0) Gecko/20100101 　　Firefox/50.0");
	        DesireCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "D:\\software\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
	        DesireCaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
	        WebDriver driver=new PhantomJSDriver(DesireCaps);
	       
	       
	        driver.get("https://online.immi.gov.au/lusc/login");
	        Thread.sleep(1000*10l);
	        WebDriverWait wait = new WebDriverWait(driver, 20);
	        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("_2b0a0a3a1a_input")));
	        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            System.out.println(driver.getPageSource());
            WebElement ele = driver.findElement(By.id("_2b0a0a3a1a_input"));
            ele.sendKeys("rogerfan");
            driver.findElement(By.id("_2b0a0a3b1a")).sendKeys("Success1987");
            driver.findElement(By.id("_2b0a0a4a1a0")).click();
            Thread.sleep(5000L);
            driver.findElement(By.id("_2b0a0b0e0b0a")).click();
            Thread.sleep(5000L);
	        System.out.println(driver.getPageSource());
	}
}
