package com.yr.visa.proxy;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.yr.visa.proxy.ctx.Brower;


public class LoginProxyTest extends BasicConfigTest {
	LoginProxy loginProxy = new LoginProxy();
	Brower b = null;
	@Before
	public void  init(){
		BasicConfigTest.man.cleanupTmpPath();
		loginProxy.setConfigManager(BasicConfigTest.man);
	}
	
	@Test
	public void testLogin() {
		b =loginProxy.login();
	}
	@After
	public void cleanup(){
		if(b != null){
			b.getDriver().quit();
		}
	}
}
